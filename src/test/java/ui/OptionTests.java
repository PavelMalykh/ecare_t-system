package ui;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class OptionTests {

    private SelenideElement loginOnMainPageElement = $("[id='login']");
    private SelenideElement emailOnLoginPage = $("[id='email']");
    private SelenideElement passwordOnLoginPage = $("[id='password']");
    private SelenideElement submitOnLoginPage = $("[id='submit']");
    private SelenideElement adminPanel = $("[id='admin-panel']");
    private SelenideElement optionMenu = $("[id='options']");
    private SelenideElement createNewOptionButton = $("[id='create']");
    private SelenideElement optionNameInput = $("[id='optionName']");
    private SelenideElement optionPriceInput = $("[id='optionPrice']");
    private SelenideElement optionConnectionCostInput = $("[id='optionConnectionPrice']");
    private SelenideElement submitOptionCreation = $("[id='submit']");
    private SelenideElement successAlert = $("[id='option-success-alert']");
    private SelenideElement returnToOptionsPageButton = $("[id='return']");

    @Test(testName = "Create option")
    public void createOption() {
        String host = "http://localhost:8082/eecare_war/";
        String adminLogin = "pasha@mail.ru";
        String adminPassword = "123";
        String optionName = "autotest_name";
        String optionPrice = "1";
        String optionConnectionCost = "2";

        Selenide.open(host);
        loginOnMainPageElement.shouldBe(visible);
        loginOnMainPageElement.click();
        emailOnLoginPage.shouldBe(visible);
        emailOnLoginPage.val(adminLogin);
        passwordOnLoginPage.val(adminPassword);
        Selenide.sleep(1500);
        submitOnLoginPage.click();
        adminPanel.shouldBe(visible);
        optionMenu.click();
        Selenide.sleep(1500);
        createNewOptionButton.click();
        Selenide.sleep(1500);
        optionNameInput.val(optionName);
        optionPriceInput.val(optionPrice);
        optionConnectionCostInput.val(optionConnectionCost);
        Selenide.sleep(1500);
        submitOptionCreation.click();
        successAlert.shouldBe(visible);
        Selenide.sleep(1500);
        returnToOptionsPageButton.click();
        $("[id='item'][data-name='" + optionName + "']").shouldBe(visible);
    }
}