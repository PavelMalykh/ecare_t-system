package unit.customer;

import com.tsystem.ecare.entity.Contract;
import com.tsystem.ecare.entity.Customer;
import com.tsystem.ecare.entity.Tariff;
import com.tsystem.ecare.exception.EmptyDataException;
import com.tsystem.ecare.exception.EmptyNumberException;
import com.tsystem.ecare.exception.NotFoundException;
import com.tsystem.ecare.exception.PhoneAlreadyBusyException;
import com.tsystem.ecare.service.ContractService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

public class ContractTests {

    @Mock
    private Contract contract;

    @InjectMocks
    private ContractService mockedContractService = mock(ContractService.class);

    @Test
    @DisplayName("Create contract")
    public void createContract() {
        when(mockedContractService.createContract(spy(new Customer()), spy(new Tariff()), Collections.EMPTY_SET, "8-800")).thenReturn(1L);

        contract = spy(new Contract());
        Tariff tariff = spy(new Tariff());
        Customer customer = spy(new Customer());
        String phone = "8-800";
        Set<Long> options = Collections.EMPTY_SET;

        mockedContractService.createContract(customer, tariff, options, phone);
        verify(mockedContractService, times(1)).createContract(customer, tariff, options, phone);

        Assertions.assertEquals(0, mockedContractService.createContract(customer, tariff, options, phone));
    }

    @Test
    @DisplayName("Get one")
    public void getContract() {
        when(mockedContractService.getOne(anyLong())).thenReturn(new Contract());
        mockedContractService.getOne(1L);
        verify(mockedContractService, times(1)).getOne(1L);
    }

    @Test
    @DisplayName("Not found contract exception")
    public void notFoundContract() {
        when(mockedContractService.getOne(anyLong())).thenThrow(NotFoundException.class);
        Assertions.assertThrows(NotFoundException.class, () -> mockedContractService.getOne(anyLong()));
    }

    @Test
    @DisplayName("Get all contracts")
    public void getAllContracts() {
        when(mockedContractService.getOne(anyLong())).thenThrow(NotFoundException.class);
        Assertions.assertThrows(NotFoundException.class, () -> mockedContractService.getOne(anyLong()));

        when(mockedContractService.getAll()).thenReturn(Collections.emptyList());
        mockedContractService.getAll();
        verify(mockedContractService, times(1)).getAll();
        Assertions.assertEquals(0, mockedContractService.getAll().size());
    }

    @Test
    @DisplayName("Get phone")
    public void getPhone() {
        when(mockedContractService.getPhone("8-800")).thenReturn("00");
        mockedContractService.getPhone("8-800");
        verify(mockedContractService, times(1)).getPhone("8-800");
        Assertions.assertEquals("00", mockedContractService.getPhone("8-800"));
    }

    @Test
    @DisplayName("Check phone")
    public void checkPhone() {
        when(mockedContractService.checkPhone("8-800")).thenReturn(true);
        mockedContractService.getPhone("8-800");
        verify(mockedContractService, times(1)).getPhone("8-800");
        Assertions.assertEquals(true, mockedContractService.checkPhone("8-800"));
    }

    @Test
    @DisplayName("Check phone already busy exception")
    public void checkPhoneAlreadyBusyException() {
        when(mockedContractService.checkPhone(anyString())).thenThrow(PhoneAlreadyBusyException.class);
        Assertions.assertThrows(PhoneAlreadyBusyException.class, () -> mockedContractService.checkPhone("8-800"));
    }

    @Test
    @DisplayName("Check empty phone exception")
    public void checkEmptyPhoneException() {
        when(mockedContractService.checkPhone(anyString())).thenThrow(EmptyNumberException.class);
        Assertions.assertThrows(EmptyNumberException.class, () -> mockedContractService.checkPhone("8-800"));
    }

    @Test
    @DisplayName("Change options empty contract")
    public void changeOptionsEmptyContract() {
        doThrow(NotFoundException.class).when(mockedContractService).changeOptions(anyLong(), anySet());
        Assertions.assertThrows(NotFoundException.class, () -> mockedContractService.changeOptions(1L, new HashSet<>()));
    }

    @Test
    @DisplayName("Change options empty options")
    public void changeOptionsEmptyOptions() {
        doThrow(EmptyDataException.class).when(mockedContractService).changeOptions(anyLong(), anySet());
        Assertions.assertThrows(EmptyDataException.class, () -> mockedContractService.changeOptions(1L, new HashSet<>()));
    }
}