package unit.customer;

import com.tsystem.ecare.dto.UserDTO;
import com.tsystem.ecare.entity.Customer;
import com.tsystem.ecare.exception.EmailAlreadyBusyException;
import com.tsystem.ecare.exception.NotFoundException;
import com.tsystem.ecare.service.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CustomerTests {

    @Mock
    private UserDTO customer;

    @InjectMocks
    private CustomerService mockedCustomerService = mock(CustomerService.class);

    @Test
    @DisplayName("Create customer")
    public void createCustomer() {
        when(mockedCustomerService.createCustomer(any())).thenReturn(1L);

        customer = new UserDTO();
        customer.setFirstName("Test");
        customer.setLastName("Bot");
        customer.setAddress("SPB");
        customer.setPassportData("passport");
        customer.setPassword("123");
        Assertions.assertEquals(1L, mockedCustomerService.createCustomer(customer));
    }

    @Test
    @DisplayName("Already taken customer name")
    public void alreadyTakenEmailException() {
        customer = new UserDTO();
        customer.setFirstName("Test");
        customer.setLastName("Bot");
        customer.setAddress("SPB");
        customer.setPassportData("passport");
        customer.setPassword("123");
        when(mockedCustomerService.createCustomer(customer)).thenThrow(EmailAlreadyBusyException.class);
        assertThrows(EmailAlreadyBusyException.class, () -> mockedCustomerService.createCustomer(customer));
    }

    @Test
    @DisplayName("Get one customer")
    public void getOneCustomer() {
        when(mockedCustomerService.getOne(anyLong())).thenReturn(spy(new Customer()));
        mockedCustomerService.getOne(1L);
        verify(mockedCustomerService, times(1)).getOne(1L);
    }

    @Test
    @DisplayName("Get all customers")
    public void getAllCustomers() {
        when(mockedCustomerService.getAll()).thenReturn(Collections.emptyList());
        mockedCustomerService.getAll();
        verify(mockedCustomerService, times(1)).getAll();
        Assertions.assertEquals(0, mockedCustomerService.getAll().size());
    }

    @Test
    @DisplayName("Get customer by user id")
    public void getCustomerByUserId() {
        when(mockedCustomerService.getByUserId(anyLong())).thenReturn(spy(new Customer()));
        mockedCustomerService.getByUserId(1L);
        verify(mockedCustomerService, times(1)).getByUserId(1L);
    }

    @Test
    @DisplayName("Not found user exception")
    public void userNotFoundException() {
        when(mockedCustomerService.getOne(anyLong())).thenThrow(NotFoundException.class);
        Assertions.assertThrows(NotFoundException.class, () -> mockedCustomerService.getOne(anyLong()));
    }

    @Test
    @DisplayName("Change user status")
    public void changeStatus() {
        doNothing().when(mockedCustomerService).changeStatus(anyString(), anyBoolean());
        verify(mockedCustomerService, never()).changeStatus("test@mail.ru", false);
    }
}