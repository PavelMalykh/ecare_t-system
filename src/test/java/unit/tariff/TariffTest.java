package unit.tariff;

import com.tsystem.ecare.entity.Tariff;
import com.tsystem.ecare.exception.AlreadyCreatedException;
import com.tsystem.ecare.exception.NotFoundException;
import com.tsystem.ecare.service.TariffService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TariffTest {

    @Mock
    private Tariff tariff;

    @InjectMocks
    private TariffService mockedTariffService = mock(TariffService.class);

    @Test
    @DisplayName("Create tariff")
    public void createTariff() {
        when(mockedTariffService.createTariff(any())).thenReturn(1L);

        tariff = new Tariff();
        tariff.setId(1L);
        tariff.setName("Test");
        tariff.setPrice(new BigDecimal(10));
        Assertions.assertEquals(1L, mockedTariffService.createTariff(tariff));
    }

    @Test
    @DisplayName("Already taken tariff name")
    public void alreadyTakenTariffNameException() {
        tariff = new Tariff();
        tariff.setId(298L);
        tariff.setName("Test");
        tariff.setPrice(new BigDecimal(10));
        when(mockedTariffService.createTariff(tariff)).thenThrow(AlreadyCreatedException.class);

        assertThrows(AlreadyCreatedException.class, () -> mockedTariffService.createTariff(tariff));
    }

    @Test
    @DisplayName("Get one tariff")
    public void getOneTariff() {
        when(mockedTariffService.getOne(anyLong())).thenReturn(spy(new Tariff()));
        mockedTariffService.getOne(1L);
        verify(mockedTariffService, times(1)).getOne(1L);
    }

    @Test
    @DisplayName("Get all tariff")
    public void getAllTariffs() {
        when(mockedTariffService.getAll()).thenReturn(Collections.emptyList());
        mockedTariffService.getAll();
        verify(mockedTariffService, times(1)).getAll();
        Assertions.assertEquals(0, mockedTariffService.getAll().size());
    }

    @Test
    @DisplayName("Get tariff by name")
    public void getTariffByName() {
        when(mockedTariffService.getTariffByName(anyString())).thenReturn(spy(new Tariff()));
        mockedTariffService.getTariffByName("Test");
        verify(mockedTariffService, times(1)).getTariffByName(anyString());
    }

    @Test
    @DisplayName("Not found tariff exception")
    public void tariffNotFoundException() {
        when(mockedTariffService.getOne(anyLong())).thenThrow(NotFoundException.class);
        Assertions.assertThrows(NotFoundException.class, () -> mockedTariffService.getOne(anyLong()));
    }
}