package unit.option;

import com.tsystem.ecare.entity.Option;
import com.tsystem.ecare.exception.AlreadyCreatedException;
import com.tsystem.ecare.exception.NotFoundException;
import com.tsystem.ecare.service.OptionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class OptionTests {

    @Mock
    private Option option;

    @InjectMocks
    private OptionService mockedOptionService = mock(OptionService.class);

    @Test
    @DisplayName("Create option")
    public void createOption() {
        when(mockedOptionService.createOption(any())).thenReturn(1L);

        option = new Option();
        option.setName("Test");
        option.setPrice(new BigDecimal(1));
        option.setConnectionCost(new BigDecimal(2));
        Assertions.assertEquals(1L, mockedOptionService.createOption(option));
    }

    @Test
    @DisplayName("Already taken tariff name")
    public void alreadyTakenOptionNameException() {
        option = new Option();
        option.setName("Test");
        option.setPrice(new BigDecimal(1));
        option.setConnectionCost(new BigDecimal(2));

        when(mockedOptionService.createOption(option)).thenThrow(AlreadyCreatedException.class);
        assertThrows(AlreadyCreatedException.class, () -> mockedOptionService.createOption(option));
    }

    @Test
    @DisplayName("Get all options")
    public void getAllOptions() {
        when(mockedOptionService.getAll()).thenReturn(Collections.emptyList());
        mockedOptionService.getAll();
        verify(mockedOptionService, times(1)).getAll();
        Assertions.assertEquals(0, mockedOptionService.getAll().size());
    }

    @Test
    @DisplayName("Get one option")
    public void getOneOption() {
        when(mockedOptionService.getOne(anyLong())).thenReturn(spy(new Option()));
        mockedOptionService.getOne(1L);
        verify(mockedOptionService, times(1)).getOne(anyLong());
    }

    @Test
    @DisplayName("Not found option exception")
    public void notFoundOptionException() {
        when(mockedOptionService.getOne(anyLong())).thenThrow(NotFoundException.class);
        assertThrows(NotFoundException.class, () -> mockedOptionService.getOne(1L));
    }

    @Test
    @DisplayName("Get all options by Ids")
    public void getAllOptionsByIds() {
        when(mockedOptionService.getAllByIds(anySet())).thenReturn(Collections.EMPTY_SET);
        mockedOptionService.getAllByIds(new HashSet<>());
        verify(mockedOptionService, times(1)).getAllByIds(new HashSet<>());
        Assertions.assertEquals(0, mockedOptionService.getAllByIds(new HashSet<>()).size());
    }

    @Test
    @DisplayName("Check available option name")
    public void checkAvailableOptionName() {
        when(mockedOptionService.isNameAvailable(spy(new Option()))).thenReturn(false);
        mockedOptionService.isNameAvailable(new Option());
        verify(mockedOptionService, times(1)).isNameAvailable(new Option());
        Assertions.assertFalse(mockedOptionService.isNameAvailable(option));
    }

    @Test
    @DisplayName("Get incompatible options")
    public void getIncompatibleOptions() {
        when(mockedOptionService.getIncompatibleOptions(anyLong())).thenReturn(Collections.EMPTY_SET);
        mockedOptionService.getIncompatibleOptions(1L);
        verify(mockedOptionService, times(1)).getIncompatibleOptions(1L);
        Assertions.assertEquals(0 , mockedOptionService.getIncompatibleOptions(1L).size());
    }
}