package com.tsystem.ecare.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity(name = "option")
@Table(name = "option")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@JsonIgnoreProperties({"obligatory", "incompatible"})
public class Option {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    @NotBlank(message = "Name should not be empty")
    @Size(max = 30, message = "Name should be less or equal than 30 symbols")
    private String name;

    @Column
    private Boolean deleted;

    @Column
    @NotNull(message = "Price should not be empty")
    @DecimalMax(value = "500", message = "Value should be les than 500")
//    @Digits(integer = 3, fraction = 2, message = "Value should be less than 500 and consists of maximum 2 decimal places")
    private BigDecimal price;

    @Column
    @NotNull(message = "Connection cost should not be empty")
    @DecimalMax(value = "300", message = "Value should be les than 300")
//    @Digits(integer = 3, fraction = 2, message = "Value should be les than 300 and consists of maximum 2 symbols")
    private BigDecimal connectionCost;

    @ManyToMany(cascade = CascadeType.MERGE,
            fetch = FetchType.EAGER)
//    @ToString.Exclude
    @JsonProperty("obligatory")
    @JoinTable(name= "obligatory_option",
            joinColumns = { @JoinColumn(name= "option_id") },
            inverseJoinColumns = { @JoinColumn(name="obligatory_option_id") })
    private Set<Option> obligatoryOption;

    @ManyToMany(cascade = CascadeType.MERGE,
            fetch = FetchType.EAGER)
    @ToString.Exclude
//    @JsonIgnoreProperties
    @JsonProperty("incompatible")
    @JoinTable(name= "incompatible_option",
            joinColumns = { @JoinColumn(name= "option_id") },
            inverseJoinColumns = { @JoinColumn(name="incompatible_option_id") })
    private Set<Option> incompatibleOption;

    public Set<Option> getObligatoryOption() {
        return obligatoryOption;
    }

    public void setObligatoryOption(Set<Option> obligatoryOption) {
        this.obligatoryOption = obligatoryOption;
    }

    public Set<Option> getIncompatibleOption() {
        return incompatibleOption;
    }

    public void setIncompatibleOption(Set<Option> incompatibleOption) {
        this.incompatibleOption = incompatibleOption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Option option = (Option) o;
        return Objects.equals(id, option.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}