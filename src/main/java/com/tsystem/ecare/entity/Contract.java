package com.tsystem.ecare.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "contract")
@Table(name = "contract")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Contract {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String phone;

    @Column
    private Boolean deleted;

    @Column(name = "deleted_by")
    private String deletedBy;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id")
    @ToString.Exclude
    private Customer customer;

    @OneToOne
    @JoinColumn(name = "tariff_id")
    private Tariff tariff;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Option> options;
}