package com.tsystem.ecare.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

import java.util.*;

@Entity(name = "tariff")
@Table(name = "tariff")
@ToString
@Getter
@Setter
public class Tariff {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @NotBlank(message = "Name should not be empty")
    @Size(max = 30, message = "Name should be less or equal than 30 symbols")
    private String name;

    @Column
    @NotNull(message = "Price should not be empty")
    @DecimalMax(value = "1000", message = "Value should be les than 1000")
    @Digits(integer = 4, fraction = 2, message = "Value should be les than 1000 and consists of maximum 2 decimal places")
    private BigDecimal price;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "tariff_option", joinColumns = @JoinColumn (name = "tariff_id"), inverseJoinColumns = @JoinColumn (name = "option_id"))
    private Set<Option> optionSet;

    @Column
    private Boolean deleted;

    public void setOptionSet(Set<Option> optionList) {
        optionSet.clear();
        this.optionSet.addAll(optionList);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tariff tariff = (Tariff) o;
        return id.equals(tariff.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}