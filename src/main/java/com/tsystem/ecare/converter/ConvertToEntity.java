package com.tsystem.ecare.converter;

import com.tsystem.ecare.dto.UserDTO;
import com.tsystem.ecare.entity.Customer;
import org.springframework.beans.BeanUtils;

public class ConvertToEntity {

    public static Object Converter(UserDTO userDTO, Customer customer) {
        BeanUtils.copyProperties(userDTO, customer);
        return customer;
    }
}