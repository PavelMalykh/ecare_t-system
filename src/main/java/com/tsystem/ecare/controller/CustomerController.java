package com.tsystem.ecare.controller;

import com.tsystem.ecare.dto.UserDTO;
import com.tsystem.ecare.entity.Customer;
import com.tsystem.ecare.service.ContractService;
import com.tsystem.ecare.service.CustomerService;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ContractService contractService;

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("customers", customerService.getAll());
        return "customers";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable("id") Long id, Model model) {
        model.addAttribute("customer", customerService.getOne(id));
        return "customer";
    }

    @GetMapping("/creation")
    public String openCreationPage(Model model) {
        model.addAttribute("customer", new UserDTO());
        return "customerCreation";
    }

    @PostMapping
    public String createCustomer(Model model, @ModelAttribute("customer") @Valid UserDTO customer, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "customerCreation";
        }
        Long customerId = customerService.createCustomer(customer);
        model.addAttribute("customerId", customerId);
        return "customerSuccess";
    }

    @PostMapping("/block")
    public String blockUser(@RequestParam(value = "email") String email, @RequestParam(value = "isActive") Boolean isActive) {
        customerService.changeStatus(email, isActive);
        return "customer";
    }

    @GetMapping("/profile")
    public String myProfile(HttpServletRequest request, Model model) {
        Customer customer = customerService.getByUserId(request);
        model.addAttribute("customer", customer);
        return "customer";
    }

    @PostMapping("/update")
    public String updateCustomer(@Valid UserDTO userDTO, @RequestParam(value = "id") Long customerId) {
        String email = userDTO.getEmail();

        customerService.updateCustomer(customerId, userDTO);
        return "customer";
    }

    @PostMapping("/phone")
    public String searchByPhone(@RequestParam(value = "phone", required = false) String phone, Model model) {
        Customer customer = contractService.getOneByPhone(phone).getCustomer();
        model.addAttribute("customer", customer);
        return "customer";
    }
}