package com.tsystem.ecare.controller;

import com.tsystem.ecare.exception.BlockedUserException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/login-error")
public class LoginController {

    @GetMapping
    public String login(HttpServletRequest request) {
        Throwable exception = (Throwable) request.getSession().getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
        if (exception.toString().contains("BadCredentialsException")) {
            return "incorrectLoginOrPassword";
        }
        Throwable exceptionCause = exception.getCause();
        if (exceptionCause instanceof BlockedUserException) {
            return "forbidden";
        }
        return "login";
    }
}