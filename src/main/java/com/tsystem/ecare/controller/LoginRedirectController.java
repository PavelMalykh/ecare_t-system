package com.tsystem.ecare.controller;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/default")
public class LoginRedirectController {

    @GetMapping
    public String defaultAfterLogin(HttpServletRequest request) {
        Object userPrincipal = request.getUserPrincipal();
        Boolean isAdmin = ((UsernamePasswordAuthenticationToken) userPrincipal).getAuthorities().stream().anyMatch(ga -> ga.getAuthority().equals("ADMIN"));
        if (isAdmin) {
            return "redirect:/adminPage";
        }
        return "redirect:/userPage";
    }
}