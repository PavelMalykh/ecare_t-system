package com.tsystem.ecare.controller;

import com.tsystem.ecare.dto.ContractDTO;
import com.tsystem.ecare.entity.Contract;
import com.tsystem.ecare.entity.Customer;
import com.tsystem.ecare.entity.Tariff;
import com.tsystem.ecare.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Set;

@Controller
@RequestMapping("/contract")
public class ContractController {

    private static final Logger log = Logger.getLogger(ContractController.class);

    @Autowired
    private ContractService contractService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TariffService tariffService;

    @Autowired
    private OptionService optionService;

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("contracts", contractService.getAll());
        return "contracts";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable("id") Long id, Model model) {
        log.info("Open contract id " + id);
        model.addAttribute("contract", contractService.getOne(id));
        return "contract";
    }

    @PostMapping
    public String createContract(ContractDTO contract, Model model) {
        Tariff tariff = tariffService.getOne(contract.getTariff());
        Customer customer = customerService.getOne(contract.getCustomer());

        Long createdContractID = contractService.createContract(customer, tariff, contract.getOptions(), contract.getPhone());
        model.addAttribute("contractId", createdContractID);
        return "contactCreation";
    }

    @GetMapping("/mine")
    public String myContract(HttpServletRequest request, Model model) {
        Customer customer = customerService.getByUserId(request);
        model.addAttribute("contracts", customer.getContract());
        return "myContract";
    }

    @PostMapping("/{id}")
    public String changeTariff(@PathVariable("id") Long contractId, @RequestParam(value = "tariffId") Long tariffId) {
        contractService.changeTariff(contractId, tariffId);
        return "contract";
    }

    @GetMapping("/changeTariff")
    public String changeTariffPage(Model model) {
        model.addAttribute("tariffs", tariffService.getAll());
        return "changeContract";
    }

    @GetMapping("/changeOption")
    public String changeOptionsPage() {
        return "changeOptionContract";
    }

    @PostMapping("/changeOptions")
    public String changeOptions(@RequestParam(value = "contractId") Long contractId, @RequestParam(value = "options") Set<Long> options) {
        contractService.changeOptions(contractId, options);
        return "contract";
    }

    @PostMapping("/block")
    public String block(@RequestParam(value = "contractId") Long contractId, HttpServletRequest request) {
        contractService.block(contractId, request);
        return "contract";
    }

    @GetMapping("/creation/firstStep")
    public String getWizardFirstPage(@RequestParam(value = "customerId") Long customerId, Model model) {
        model.addAttribute("tariffs", tariffService.getAll());
        model.addAttribute("customer", customerService.getOne(customerId));
        return "contractCreationWizardOne";
    }

    @GetMapping("/creation/secondStep")
    public String getWizardSecondPage(@RequestParam(value = "tariffId") Long tariffId, Model model) {
        model.addAttribute("tariff", tariffService.getOne(tariffId));
        return "contractCreationWizardSecond";
    }

    @GetMapping("/creation/thirdStep")
    public String getWizardThirdPage(@RequestParam(value = "options") Set<Long> options, Model model) {
        model.addAttribute("selectedOptions", optionService.getAllByIds(options));
        return "contractCreationWizardThird";
    }
}