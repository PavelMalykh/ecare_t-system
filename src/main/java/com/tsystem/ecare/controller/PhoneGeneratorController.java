package com.tsystem.ecare.controller;

import com.tsystem.ecare.entity.Contract;
import com.tsystem.ecare.entity.Customer;
import com.tsystem.ecare.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/phone")
public class PhoneGeneratorController {

    @Autowired
    private ContractService contractService;

    @PostMapping("/generate")
    public String generatePhone(@RequestParam(value = "phone", required = false) String phone, Model model) {
         String x = contractService.getPhone(phone);
         model.addAttribute("phone", x);
         return "contactCreation";
    }

    @GetMapping
    public String checkPhone(@RequestParam(value = "phone", required = false) String phone, Model model) {
        Boolean isOk = contractService.checkPhone(phone);
        model.addAttribute("isOk", isOk);
        return "contactCreation";
    }
}