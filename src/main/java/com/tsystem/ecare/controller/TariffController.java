package com.tsystem.ecare.controller;

import com.tsystem.ecare.entity.Option;
import com.tsystem.ecare.entity.Tariff;
import com.tsystem.ecare.service.OptionService;
import com.tsystem.ecare.service.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@Controller
@RequestMapping("/tariff")
public class TariffController {

    @Autowired
    private TariffService tariffService;

    @Autowired
    private OptionService optionService;

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("tariffs", tariffService.getAll());
        return "tariffs";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable("id") Long id, Model model) {
        model.addAttribute("tariff", tariffService.getOne(id));
        return "tariff";
    }

    @GetMapping("/creation")
    public String openCreationPage(Model model) {
        model.addAttribute("tariff", new Tariff());
        return "tariffCreation";
    }

    @PostMapping
    public String createTariff(@ModelAttribute("tariff") @Valid Tariff tariff, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "tariffCreation";
        }
        Long createdTariffId = tariffService.createTariff(tariff);
        model.addAttribute("id", createdTariffId);
        return "tariffSuccess";
    }

    @PostMapping("/block/{id}")
    public String changeStatus(@PathVariable("id") Long id) {
        tariffService.changeStatus(id);
        return "tariff";
    }

    @PostMapping("/linking")
    public String linkingTariff(@RequestParam(value = "tariff") Long tariffId, @RequestParam(value = "options") Set<Long> options) {
        Tariff tariff = tariffService.getOne(tariffId);
        tariffService.addTariffOptions(tariff, options);
        return "tariffLinking";
    }

    @GetMapping("/linking")
    public String linkingPage(Model model) {
        model.addAttribute("options", optionService.getAll());
        return "tariffLinkingPage";
    }

    @PostMapping("/{id}")
    public String updateTariffOptions(@RequestParam(value = "tariffId") Long tariffId, @RequestParam(value = "options") Set<Long> options) {
        tariffService.updateTariffOptions(tariffId, options);
        return "tariff";
    }

    @PostMapping("/changed/{id}")
    public String updateTariff(Tariff tariff) {
        tariffService.updateTariff(tariff);
        return "tariff";
    }
}