package com.tsystem.ecare.controller;

import com.tsystem.ecare.entity.Tariff;
import com.tsystem.ecare.service.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AdvertisementController {

    @Autowired
    private TariffService tariffService;

    @GetMapping("/advertisement")
    public List<Tariff> getTariffs() {
        return tariffService.getAll();
    }

    @GetMapping("/advertisement/{id}")
    public Tariff getTariff(@PathVariable("id") Long tariffId) {
        return tariffService.getOne(tariffId);
    }
}