package com.tsystem.ecare.controller;

import com.tsystem.ecare.entity.Option;
import com.tsystem.ecare.service.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@Controller
@RequestMapping("/option")
public class OptionController {

    @Autowired
    private OptionService optionService;

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("options", optionService.getAll());
        return "options";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable("id") Long id, Model model) {
        model.addAttribute("option", optionService.getOne(id));
        return "option";
    }

    @PostMapping
    public String createOption(@ModelAttribute("option") @Valid Option option, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "optionCreation";
        }
        Long createdOptionId = optionService.createOption(option);
        model.addAttribute("option", optionService.getOne(createdOptionId));
        return "optionSuccess";
    }

    @GetMapping("/creation")
    public String openCreationPage(Model model) {
        model.addAttribute("option", new Option());
        return "optionCreation";
    }

    @GetMapping("/rule/obligatory")
    public String openObligatoryPage(Model model) {
        model.addAttribute("options", optionService.getAll());
        return "optionRuleObligatory";
    }

    @GetMapping("/rule/incompatible")
    public String openIncompatiblePage(Model model) {
        model.addAttribute("options", optionService.getAll());
        return "optionRuleIncompatible";
    }

    @PostMapping("/rule")
    public String createRule(@RequestParam(value = "option") Long optionId, @RequestParam(value = "obligatoryOptions") Set<Long> obligatoryOptions, @RequestParam(value = "incompatibleOptions") Set<Long> incompatibleOptions) {
        optionService.createRule(optionId, obligatoryOptions, incompatibleOptions);
        return "optionRuleIncompatible";
    }

    @PostMapping("/{id}")
    public String deleteOption(@PathVariable("id") Long id, Model model) {
        optionService.deleteOption(id);
        model.addAttribute("option", optionService.getOne(id));
        return "option";
    }

    @GetMapping("/change")
    public String changeOption(Model model) {
        model.addAttribute("options", optionService.getAll());
        return "changeOptionTariff";
    }

    @PostMapping("/update")
    public String updateOption(@ModelAttribute("option") @Valid Option option, BindingResult bindingResult, Model model) {
        if (!optionService.isNameAvailable(option)) {
            bindingResult.addError(new ObjectError("nameEmptyError","Name should be unique"));
            model.addAttribute("nameEmptyError", "Name should be unique");
        }
        if (bindingResult.hasErrors()) {
            return "option";
        }
        optionService.updateOption(option);
        return "redirect:/option/" + option.getId();
    }

    @GetMapping("/getOptions/{id}")
    public String getIncompatibleAndObligatoryOptions(@PathVariable("id") Long id, Model model) {
        Set<Option> incompatibleOptions = optionService.getIncompatibleOptions(id);
        Set<Option> obligatoryOptions = optionService.getObligatoryOptions(id);
        model.addAttribute("obligatoryOptions", obligatoryOptions);
        model.addAttribute("incompatibleOptions", incompatibleOptions);

        return "optionsForTariff";
    }
}