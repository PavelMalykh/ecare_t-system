package com.tsystem.ecare.dao;

import com.tsystem.ecare.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

@Repository
public class UserDAO implements Dao<User>{
    @PersistenceContext
    private EntityManager entityManager;

    public User getByEmail(String email) {
        Query query = entityManager.createQuery("select e from users e where e.email = :email");
        query.setParameter("email", email);

        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        else if (results.size() == 1)  {
            return (User) results.get(0);
        }
        throw new NonUniqueResultException();
    }

    @Override
    public User getOne(long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public List<User> getAll() {
        Query query = entityManager.createQuery("select e from customer e ");
        return query.getResultList();
    }

    @Override
    public Long save(User user) {
        entityManager.persist(user);
        return user.getId();
    }

    @Override
    public void update(User user) {
        entityManager.merge(user);
    }

    @Override
    public void delete(User user) {
        entityManager.remove(user);
    }
}
