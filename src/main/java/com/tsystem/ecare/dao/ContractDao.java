package com.tsystem.ecare.dao;

import com.tsystem.ecare.entity.Contract;
import com.tsystem.ecare.entity.Customer;
import com.tsystem.ecare.entity.Option;
import com.tsystem.ecare.entity.Tariff;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ContractDao implements Dao<Contract>{

    @PersistenceContext
    private EntityManager entityManager;

    public Contract getOneByPhoneNumber(String phone) {
        Query query = entityManager.createQuery("select e from contract e where e.phone = :phone");
        query.setParameter("phone", phone);
        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        else if (results.size() == 1)  {
            return (Contract) results.get(0);
        }
        throw new NonUniqueResultException();
    }

    @Override
    public Contract getOne(long id) {
        return entityManager.find(Contract.class, id);
    }

    @Override
    public List<Contract> getAll() {
        Query query = entityManager.createQuery("select e from contract e ");
        return query.getResultList();
    }

    @Override
    public Long save(Contract contract) {
        entityManager.persist(contract);
        return contract.getId();
    }

    @Override
    public void update(Contract contract) {
        entityManager.merge(contract);
    }

    @Override
    public void delete(Contract contract) {

    }
}