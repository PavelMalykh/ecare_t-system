package com.tsystem.ecare.dao;

import java.util.List;

public interface Dao<T> {

    T getOne(long id);

    List<T> getAll();

    Long save(T t);

    void update(T t);

    void delete(T t);
}