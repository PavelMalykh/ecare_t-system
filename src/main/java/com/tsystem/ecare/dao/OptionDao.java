package com.tsystem.ecare.dao;

import com.tsystem.ecare.entity.Option;
import com.tsystem.ecare.entity.Tariff;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class OptionDao implements Dao<Option> {
    @PersistenceContext
    private EntityManager entityManager;

    public Option getOptionByName(String name) {
        Query query = entityManager.createQuery("select e from option e where e.name = :name");
        query.setParameter("name", name);

        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        else if (results.size() == 1)  {
            return (Option) results.get(0);
        }
        throw new NonUniqueResultException();
    }

    @Override
    public Option getOne(long id) {
        return entityManager.find(Option.class, id);
    }

    @Override
    public List<Option> getAll() {
        Query query = entityManager.createQuery("select e from option e ");
        return query.getResultList();
    }

    public Set<Option> getAllByIds(Set<Long> ids) {
        List<Option> query = entityManager
                .createQuery("select e from option e where e.id IN :ids")
                .setParameter("ids", ids).getResultList();
        return new HashSet<>(query);
    }

    @Override
    public Long save(Option option) {
        entityManager.persist(option);
        return option.getId();
    }

    @Override
    public void update(Option option) {
        entityManager.merge(option);
    }

    @Override
    public void delete(Option option) {
        entityManager.remove(option);
    }
}
