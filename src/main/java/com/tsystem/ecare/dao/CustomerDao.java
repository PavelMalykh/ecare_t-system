package com.tsystem.ecare.dao;

import com.tsystem.ecare.entity.Contract;
import com.tsystem.ecare.entity.Customer;
import com.tsystem.ecare.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;

@Repository
public class CustomerDao implements Dao<Customer> {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Customer getOne(long id) {
        return entityManager.find(Customer.class, id);
    }

    public Customer getByUserId(Long id) {
        Query query = entityManager.createQuery("select e from customer e where e.user.id = :id");
        query.setParameter("id", id);
        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        else if (results.size() == 1)  {
            return (Customer) results.get(0);
        }
        throw new NonUniqueResultException();
    }

    @Override
    public List<Customer> getAll() {
        Query query = entityManager.createQuery("select e from customer e ");
        return query.getResultList();
    }

    @Override
    public Long save(Customer customer) {
        entityManager.persist(customer);
        return customer.getId();
    }

    @Override
    public void update(Customer customer) {
        entityManager.merge(customer);
    }

    @Override
    public void delete(Customer customer) {

    }
}