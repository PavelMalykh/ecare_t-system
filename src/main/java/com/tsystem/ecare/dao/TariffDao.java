package com.tsystem.ecare.dao;

import com.tsystem.ecare.entity.Option;
import com.tsystem.ecare.entity.Tariff;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Repository
public class TariffDao implements Dao<Tariff>{
    @PersistenceContext
    private EntityManager entityManager;

    public Tariff getTariffByName(String name) {
        Query query = entityManager.createQuery("select e from tariff e where e.name = :name");
        query.setParameter("name", name);

        List results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        else if (results.size() == 1)  {
            return (Tariff) results.get(0);
        }
        throw new NonUniqueResultException();
    }

    @Override
    public Tariff getOne(long id) {
        return entityManager.find(Tariff.class, id);
    }

    @Override
    public List<Tariff> getAll() {
        return entityManager.createQuery("select e from tariff e").getResultList();
    }

    @Override
    @Transactional
    public Long save(Tariff tariff) {
        entityManager.persist(tariff);
        return tariff.getId();
    }

    @Override
    public void update(Tariff tariff) {
        entityManager.merge(tariff);
    }

    @Override
    public void delete(Tariff tariff) {
        entityManager.remove(tariff);
    }
}