package com.tsystem.ecare.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(code = BAD_REQUEST, reason = "Sorry, this email is already been taken, please try another")
public class EmailAlreadyBusyException extends RuntimeException{
}