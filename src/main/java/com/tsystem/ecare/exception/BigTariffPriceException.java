package com.tsystem.ecare.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;

@ResponseStatus(code = BAD_REQUEST, reason = "Price should not be more than 1000")
public class BigTariffPriceException extends RuntimeException{
}