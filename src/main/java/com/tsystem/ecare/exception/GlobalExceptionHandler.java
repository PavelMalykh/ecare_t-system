package com.tsystem.ecare.exception;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = NotFoundException.class)
    public String handleNotFoundException(){
        return "entityNotFound";
    }

    @ExceptionHandler(value = AlreadyCreatedException.class)
    public String handleAlreadyCreatedException() { return "entityAlreadyCreated"; }

    @ExceptionHandler(value = EmptyDataException.class)
    public String handleEmptyDataException() { return "emptyData"; }

    @ExceptionHandler(value = IncorrectPhoneNumberException.class)
    public String handleIncorrectPhoneNumberException() {
        return "wrongNumber";
    }

    @ExceptionHandler(value = BlockedUserException.class)
    public String handleBlockedUserException() {
        return "forbidden";
    }

    @ExceptionHandler(value = IntersectingOptionsException.class)
    public String handleIntersectingOptionsException() {
        return "intersectingOptions";
    }

    @ExceptionHandler(value = EmailAlreadyBusyException.class)
    public String handleEmailAlreadyBusyException() {
        return "emailAlreadyTaken";
    }

    @ExceptionHandler(value = DuplicateUniqueException.class)
    public String handleDuplicateUniqueException() {
        return "duplicateUnique";
    }

    @ExceptionHandler(value = EmptyNumberException.class)
    public String handleEmptyNumberException() {
        return "emptyNumberException";
    }

    @ExceptionHandler(value = EmptyTariffException.class)
    public String handleEmptyTariffException() {
        return "emptyTariffException";
    }

    @ExceptionHandler(value = PhoneAlreadyBusyException.class)
    public String handlePhoneAlreadyByseException() {
        return "phoneAlreadyBusyException";
    }

    @ExceptionHandler(value = TariffAlreadyDeletedException.class)
    public String handleTariffAlreadyDeletedException() {
        return "deletedTariffException";
    }

    @ExceptionHandler(value = OptionAlreadyDeletedException.class)
    public String handleOptionAlreadyDeletedException() {
        return "optionAlreadyDeletedException";
    }

    @ExceptionHandler(value = BigTariffPriceException.class)
    public String handleBigTariffPriceException() {
        return "bigTariffPriceException";
    }
}