package com.tsystem.ecare.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(code = BAD_REQUEST, reason = "Sorry, one of the option was deleted")
public class OptionAlreadyDeletedException extends RuntimeException{
}