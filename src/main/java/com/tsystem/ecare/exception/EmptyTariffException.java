package com.tsystem.ecare.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(code = BAD_REQUEST, reason = "Sorry, you need to select a tariff")
public class EmptyTariffException extends RuntimeException{
}