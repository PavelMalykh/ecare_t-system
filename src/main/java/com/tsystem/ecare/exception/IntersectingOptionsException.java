package com.tsystem.ecare.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(code = BAD_REQUEST, reason = "Can't select intersecting options or can't select the same option")
public class IntersectingOptionsException extends RuntimeException{
}