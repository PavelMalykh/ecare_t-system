package com.tsystem.ecare.config;

import com.tsystem.ecare.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable().cors().disable()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/main").permitAll()
                .mvcMatchers("/tariff").permitAll()
                .mvcMatchers("/tariff/creation").hasAnyAuthority("ADMIN")
                .mvcMatchers("/option/creation").hasAnyAuthority("ADMIN")
                .mvcMatchers("/contract").hasAnyAuthority("ADMIN")
                .mvcMatchers("/phone/**").hasAnyAuthority("ADMIN")
                .mvcMatchers("/tariff/linking").hasAnyAuthority("ADMIN")
                .mvcMatchers("/customer/creation").permitAll()
                .antMatchers("/customer/**").permitAll()
                .antMatchers("/advertisement/**").permitAll()
                .mvcMatchers(HttpMethod.POST, "/customer/creation").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/scripts/**").permitAll()
                .antMatchers("/styles/**").permitAll()
                .antMatchers("/contract/mine").hasAnyAuthority("USER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/default", true)
                .failureUrl("/login-error")
                .and()
                .logout()
                .deleteCookies("JSESSIONID");
    }

    @Bean
    protected PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }

    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }

//    @Bean
//    public RoleHierarchy roleHierarchy() {
//        var hierarche = new RoleHierarchyImpl();
//        hierarche.setHierarchy("ROLE_ADMIN > ROLE_USER\nROLE_SA > ROLE_USER\nROLE_ADMIN > ROLE_SA");
//        return hierarche;
//    }
}
