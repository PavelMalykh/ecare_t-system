package com.tsystem.ecare.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiTemplate;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.tsystem.ecare"})
@Import({SecurityConfiguration.class})
public class WebConfig implements WebMvcConfigurer {

    public void configureViewResolvers(ViewResolverRegistry registry) {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();

        viewResolver.setPrefix("/WEB-INF/pages/");
        viewResolver.setSuffix(".jsp");
        registry.viewResolver(viewResolver);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/styles/**")
                .addResourceLocations("/WEB-INF/styles/");

        registry.addResourceHandler("scripts/**")
                .addResourceLocations("/WEB-INF/scripts/");

        registry.addResourceHandler("/img/**")
                .addResourceLocations("/WEB-INF/img/");

        registry.addResourceHandler("/pages/**")
                .addResourceLocations("/WEB-INF/pages/");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/main").setViewName("main");
        registry.addViewController("/").setViewName("main");
        registry.addViewController("/adminPage").setViewName("adminPage");
        registry.addViewController("/entityNotFound").setViewName("entityNotFound");
        registry.addViewController("/forbidden").setViewName("forbidden");
        registry.addViewController("/incorrectLoginOrPassword").setViewName("incorrectLoginOrPassword");
        registry.addViewController("/optionRule").setViewName("optionRule");
        registry.addViewController("/intersectingOptions").setViewName("intersectingOptions");
        registry.addViewController("/userPage").setViewName("userPage");
        registry.addViewController("/emailAlreadyTaken").setViewName("emailAlreadyTaken");
        registry.addViewController("/duplicateUnique").setViewName("duplicateUnique");
        registry.addViewController("/wrongNumber").setViewName("wrongNumber");
        registry.addViewController("/emptyNumberException").setViewName("emptyNumberException");
        registry.addViewController("/wrongNumber").setViewName("wrongNumber");
        registry.addViewController("/emptyTariffException").setViewName("emptyTariffException");
        registry.addViewController("/contract/creation/firstStep").setViewName("contractCreationWizardOne");
        registry.addViewController("/contract/creation/secondStep").setViewName("contractCreationWizardSecond");
        registry.addViewController("/contractCreationWizardThird").setViewName("contractCreationWizardThird");
        registry.addViewController("/deletedTariffException").setViewName("deletedTariffException");
        registry.addViewController("/optionAlreadyDeletedException").setViewName("optionAlreadyDeletedException");
        registry.addViewController("/bigTariffPriceException").setViewName("bigTariffPriceException");
    }
}
