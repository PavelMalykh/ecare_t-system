package com.tsystem.ecare.dto;


import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
public class UserDTO {

    @Email(message = "Incorrect email, positive email example: my_email@domain.ru")
    @NotBlank(message = "Email should not be empty")
    private String email;

//    @Size(min = 6, max = 15, message = "Password length must of between 6 and 15 characters")
    @NotBlank(message = "Password should not be empty")
    private String password;

    @Size(max = 20, message = "Name should be less than 20 characters")
    @NotBlank(message = "Name should not be empty")
    private String firstName;

    @Size(max = 20, message = "Lastname should be less than 20 characters")
    @NotBlank(message = "Lastname should not be empty")
    private String lastName;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = "Birthday should not be empty")
    private LocalDate birthday;

    @Size(max = 10, message = "Passport data length must be less equal than 10 characters")
    @NotBlank(message = "Passport data should not be empty")
    private String passportData;

    @Size(max = 100, message = "Address length must be less equal than 100 characters.")
    @NotBlank(message = "Address should not be empty")
    private String address;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPassportData() {
        return passportData;
    }

    public void setPassportData(String passportData) {
        this.passportData = passportData;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}