package com.tsystem.ecare.dto;

import java.util.Set;

public class ContractDTO {

    private String phone;
    private Long customer;
    private Long tariff;
    private Set<Long> options;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getCustomer() {
        return customer;
    }

    public void setCustomer(Long customer) {
        this.customer = customer;
    }

    public Long getTariff() {
        return tariff;
    }

    public void setTariff(Long tariff) {
        this.tariff = tariff;
    }

    public Set<Long> getOptions() {
        return options;
    }

    public void setOptions(Set<Long> options) {
        this.options = options;
    }
}