package com.tsystem.ecare.service;

import com.tsystem.ecare.converter.ConvertToEntity;
import com.tsystem.ecare.dao.CustomerDao;
import com.tsystem.ecare.dao.UserDAO;
import com.tsystem.ecare.dto.UserDTO;
import com.tsystem.ecare.entity.Customer;
import com.tsystem.ecare.entity.User;
import com.tsystem.ecare.exception.AlreadyCreatedException;
import com.tsystem.ecare.exception.EmailAlreadyBusyException;
import com.tsystem.ecare.exception.EmptyDataException;
import com.tsystem.ecare.exception.NotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class CustomerService {

    private static final Logger log = Logger.getLogger(CustomerService.class);

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    /**
     *
     * @return list of customer
     */

    @Transactional
    public List<Customer> getAll() {
        return customerDao.getAll();
    }

    /**
     * @param id customer Id
     * @throws NotFoundException in case there is no customer with the following ID
     * @return a customer
     */

    @Transactional
    public Customer getOne(Long id) {
        log.info("Get customer id: " + id);
        Customer foundedCustomer = customerDao.getOne(id);
        if (foundedCustomer == null) {
            log.info("Customer not found id: " + id);
            throw new NotFoundException();
        }
        return foundedCustomer;
    }

    /**
     * Find a customer by userId
     * @param userId user Id
     * @return customer
     */

    @Transactional
    public Customer getByUserId(Long userId) {
        log.info("Get customer by userid: " + userId);
        return customerDao.getByUserId(userId);
    }

    /**
     * Create a customer
     * @param userDTO user DTO
     * @throws EmailAlreadyBusyException in case email already taken by other user exception
     * @return customer ID
     */

    @Transactional
    public Long createCustomer(UserDTO userDTO) {
        log.info("Trying to create customer with email: " + userDTO.getEmail());
        if (userDAO.getByEmail(userDTO.getEmail()) != null) {
            log.info("Customer with email: " + userDTO.getEmail() + " has already been created");
            throw new EmailAlreadyBusyException();
        }
        Customer customer = new Customer();
        customer = (Customer) ConvertToEntity.Converter(userDTO,customer);

        User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setPassword(new BCryptPasswordEncoder().encode(userDTO.getPassword()));
        user.setRoles("USER");
        user.setEnabled(true);
        customer.setUser(user);

        userDAO.save(user);
        Long customerId = customerDao.save(customer);
        log.info("Create customer with id: " + customerId);
        return customerId;
    }

    /**
     * Make user active/inactive
     * @param email user email
     * @param isActive active/inactive user
     */

    @Transactional
    public void changeStatus(String email, Boolean isActive) {
        User user = userDAO.getByEmail(email);
        user.setEnabled(isActive);
        userDAO.update(user);
        log.info("Update customer status: " + !isActive + " with email: " + email);
    }

    /**
     * Get customer by userId
     * @param request http request to get info about user
     * @return customer
     */

    @Transactional
    public Customer getByUserId(HttpServletRequest request) {
        String userEmail = request.getUserPrincipal().getName();
        Long userId = userDetailsService.getUserEmail(userEmail).getId();
        Customer customer = customerDao.getByUserId(userId);
        log.info("Get customer by user id " + userId);
        return customer;
    }

    /**
     * Update customer
     * @param id customer ID
     * @param customerDTO user info
     * @throws NotFoundException in case if user not found
     * @throws EmailAlreadyBusyException in case if email already taken
     */

    @Transactional
    public void updateCustomer(Long id,UserDTO customerDTO) {
        log.info("Trying update customer..." + id);
        Customer customer = customerDao.getOne(id);
        if (customer == null) {
            log.info("Customer with id " + id + " not found");
            throw new NotFoundException();
        }

        if (userDAO.getByEmail(customerDTO.getEmail()) == null || userDAO.getByEmail(customerDTO.getEmail()) == customer.getUser()) {
            customer.setFirstName(customerDTO.getFirstName());
            customer.setLastName(customerDTO.getLastName());
            customer.setPassportData(customerDTO.getPassportData());
            customer.setAddress(customerDTO.getAddress());
            customer.getUser().setEmail(customerDTO.getEmail());
            customerDao.update(customer);
            log.info("Update customer: " + id);
        } else {
            log.info("Customer with email " + customerDTO.getEmail() + " has already been added");
            throw new EmailAlreadyBusyException();
        }
    }
}