package com.tsystem.ecare.service;

import com.tsystem.ecare.component.MessageSender;
import com.tsystem.ecare.dao.TariffDao;
import com.tsystem.ecare.entity.Contract;
import com.tsystem.ecare.entity.Option;
import com.tsystem.ecare.entity.Tariff;
import com.tsystem.ecare.exception.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.NameAlreadyBoundException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Stream;

import static com.tsystem.ecare.service.OptionService.checkOptionSetForInnerIntersection;

@Service
public class TariffService {

    private static final Logger log = Logger.getLogger(TariffService.class);

    @Autowired
    private MessageSender messageSender;

    @Autowired
    private TariffDao tariffDao;

    @Autowired
    private OptionService optionService;

    /**
     * Get all tariffs
     * @return List of tariffs
     */

    @Transactional
    public List<Tariff> getAll() {
        return tariffDao.getAll();
    }

    /**
     * Get one tariff
     * @param id a tariff ID
     * @throws EmptyTariffException if tariff is null
     * @throws NotFoundException if tariff not found
     * @return founded tariff
     */

    @Transactional
    public Tariff getOne(Long id) {
        if (id == null) {
            throw new EmptyTariffException();
        }
        log.info("Get tariff id " + id);
        Tariff foundedTariff = tariffDao.getOne(id);
        if (foundedTariff == null) {
            log.info("Tariff id " + id + " was not found");
            throw new NotFoundException();
        }
        return foundedTariff;
    }

    /**
     * Create tariff
     * @param tariff a tariff
     * @throws AlreadyCreatedException in case if tariff the same name was already created
     * @return created tariff ID
     */

    @Transactional
    public Long createTariff(Tariff tariff) {
        log.info("Trying to create tariff " + tariff.getId());
        if (tariffDao.getTariffByName(tariff.getName()) != null) {
            log.info("Tariff with id " + tariff.getId() + " has already been added");
            throw new AlreadyCreatedException();
        }

        tariff.setDeleted(false);
        Long tariffId = tariffDao.save(tariff);
        log.info("Tariff with id " + tariffId + " has been successfully added");
        messageSender.sendMessage("create tariff" + tariffId);
        return tariffId;
    }

    /**
     * Tariff soft delete
     * @param id tariff ID
     * @throws AssertionError in case if tariff the provided ID was not found
     */

    @Transactional
    public void changeStatus(Long id) {
        log.info("Trying to change tariff status... " + id);
        Tariff tariff = tariffDao.getOne(id);
        if (tariff == null) {
            log.info("Trying " + id + " was not found");
            throw new AssertionError("Sorry, tariff with id: " + id + " is not found");
        }
        tariff.setDeleted(!tariff.getDeleted());
        tariffDao.update(tariff);
        messageSender.sendMessage("update tariff status" + id);
        log.info("Tariff: " + id + " has been successfully changed for status:" + !tariff.getDeleted());
    }

    /**
     * Get tariff by name
     * @param name tariff name
     * @throws NotFoundException if tariff not found
     * @return founded tariff
     */

    @Transactional
    public Tariff getTariffByName(String name) {
        log.info("Get tariff by name: " + name);
        Tariff tariff = tariffDao.getTariffByName(name);
        if (tariff == null) {
            log.info("Tariff name: " + name + " was not found");
            throw new NotFoundException();
        }
        return tariff;
    }

    /**
     * Attach options to tariff
     * @param tariff tariff
     * @param options set of optionsIDs
     * @throws EmptyDataException if tariff not found
     * @throws AlreadyCreatedException in case if provided the same options set
     */

    @Transactional
    public void addTariffOptions(Tariff tariff, Set<Long> options) {
        log.info("Trying add tariff options");
        if (options == null || options.isEmpty()) {
            log.info("Options for tariff: " + tariff.getId() + " is empty");
            throw new EmptyDataException();
        }

        Set<Option> optionSet = optionService.getAllByIds(options);

        if (tariff.getOptionSet().equals(optionSet)) {
            throw new AlreadyCreatedException();
        }

        checkOptionSetForInnerIntersection(optionSet);

        tariff.setOptionSet(optionSet);
        tariffDao.update(tariff);
        messageSender.sendMessage("update");
        log.info("Add options " + options + " to tariff: " + tariff.getId());
    }

    /**
     * Update attached options
     * @param tariffId tariff ID
     * @param options set of optionsIDs
     */

    @Transactional
    public void updateTariffOptions(Long tariffId, Set<Long> options) {
        log.info("Trying update tariff..." + tariffId);
        Tariff tariff = tariffDao.getOne(tariffId);

        Set<Option> optionSet = new HashSet<>();
        if (!options.isEmpty()) {
            optionSet = optionService.getAllByIds(options);
        }

        checkOptionSetForInnerIntersection(optionSet);

        tariff.setOptionSet(optionSet);
        tariffDao.update(tariff);
        log.info("Update options" + options + " for tariff " + tariffId);

        messageSender.sendMessage("update");
    }

    /**
     * Update tariff
     * @param tariff tariff
     * @throws NotFoundException in case there is no such tariff
     * @throws BigTariffPriceException in case tariff price bigger than 1000
     * @throws DuplicateUniqueException in case if name was already created
     */

    @Transactional
    public void updateTariff(Tariff tariff) {
        if (tariff == null || tariffDao.getOne(tariff.getId()) == null) {
            throw new NotFoundException();
        }
        Tariff tariffFromDB = tariffDao.getOne(tariff.getId());

        log.info("Trying update tariff values:" + tariffFromDB.getId());

        if (tariff.getPrice().compareTo(new BigDecimal(1000)) > 0) {
            throw new BigTariffPriceException();
        }

        Tariff tariffFromDbByName = tariffDao.getTariffByName(tariff.getName());

        if (tariffFromDbByName == null || tariffFromDbByName.equals(tariffFromDB)) {
            tariffFromDB.setName(tariff.getName());
            tariffFromDB.setPrice(tariff.getPrice());

            tariffDao.update(tariffFromDB);
            log.info("Update tariff: " + tariffFromDB.getId());

            messageSender.sendMessage("update tariff" + tariffFromDB.getId());
        } else {
            throw new DuplicateUniqueException();
        }
    }
}