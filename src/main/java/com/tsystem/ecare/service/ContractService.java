package com.tsystem.ecare.service;

import com.tsystem.ecare.dao.ContractDao;
import com.tsystem.ecare.entity.Contract;
import com.tsystem.ecare.entity.Customer;
import com.tsystem.ecare.entity.Option;
import com.tsystem.ecare.entity.Tariff;
import com.tsystem.ecare.exception.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.tsystem.ecare.service.OptionService.checkOptionSetForInnerIntersection;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

@Service
public class ContractService {

    private static final Logger log = Logger.getLogger(ContractService.class);

    @Autowired
    private ContractDao contractDao;

    @Autowired
    private OptionService optionService;

    @Autowired
    private TariffService tariffService;

    /**
     *
     * @return list of contracts
     */

    @Transactional
    public List<Contract> getAll() {
        return contractDao.getAll();
    }

    /**
     *
     * @param contractId a contract ID
     * @throws NotFoundException in case there is no corresponding contract
     * @return contract at the specified ID
     */

    @Transactional
    public Contract getOne(Long contractId) {
        Contract foundedContract = contractDao.getOne(contractId);
        if (foundedContract == null) {
            throw new NotFoundException();
        }
        log.info("Get contact id: " + contractId);
        return foundedContract;
    }

    /**
     * Generate new phone number
     * @param phone phone number, string format
     * @return phone number, with the following pattern: 8-800-xxx-xx-xx
     */

    @Transactional
    public String getPhone(String phone) {
        int phoneNumberFirstPart = 3;
        int phoneNumberSecondPart = 2;
        int phoneNumberThirdPart = 2;
        log.info("Get phone: " + phone);
        if (phone.isEmpty() || contractDao.getOneByPhoneNumber(phone) == null) {
            return "8-800-" + randomNumeric(phoneNumberFirstPart) + "-" + randomNumeric(phoneNumberSecondPart) + "-" + randomNumeric(phoneNumberThirdPart);
        }
        return phone;
    }

    /**
     * Check phone number for availability
     * @param phone phone number, string format
     * @throws EmptyNumberException in case phone number is null or empty
     * @throws PhoneAlreadyBusyException in case that phone number is already taken
     * @return true/false, if phone number matches the pattern and available for taken
     */

    public boolean checkPhone(String phone) {
        if (phone == null || phone.isEmpty()) {
            throw new EmptyNumberException();
        }
        if (contractDao.getOneByPhoneNumber(phone) != null) {
            throw new PhoneAlreadyBusyException();
        }
        log.info("Check phone: " + phone);
        Pattern pattern = Pattern.compile("^8-800-(\\d{3}-\\d{2}-\\d{2})$");
        Matcher matcher = pattern.matcher(phone);
        return matcher.find();
    }

    /**
     * Create new contract
     * @param customer a customer, who owns the contract
     * @param tariff a tariff, which was selected for the contract
     * @param options a set of options IDs, which were selected for the contract
     * @param phone a phone number, which was selected for the contract
     * @throws TariffAlreadyDeletedException in case tariff already deleted
     * @throws OptionAlreadyDeletedException in case one of the options already deleted
     * @throws IncorrectPhoneNumberException in case incorrect number
     * @return created contract ID
     */

    @Transactional
    public Long createContract(Customer customer, Tariff tariff, Set<Long> options, String phone) {
        log.info("Trying create contract for customer...: " + customer.getId());
        if (checkPhone(phone)) {
            Set<Option> optionSet = optionService.getAllByIds(options);

            Set<Option> optionSetWithObligatoryOptions = new HashSet<>();

            for(Option option: optionSet) {
                optionSetWithObligatoryOptions.addAll(option.getObligatoryOption());
            }

            optionSet.addAll(optionSetWithObligatoryOptions);

            if (tariff.getDeleted()) {
                throw new TariffAlreadyDeletedException();
            }

            for (Option option: optionSet) {
                if (option.getDeleted()) {
                    throw new OptionAlreadyDeletedException();
                }
            }

            checkOptionSetForInnerIntersection(optionSet);

            Contract contract = new Contract();
            contract.setCustomer(customer);
            contract.setTariff(tariff);
            contract.setOptions(optionSet);
            contract.setPhone(phone);
            contract.setDeleted(false);
            Long id = contractDao.save(contract);
            log.info("Create contract id " + id);
            return id;
        }
        else {
            log.info("Incorrect phone for customer: " + customer.getId());
            throw new IncorrectPhoneNumberException();
        }
    }

    /**
     * Get one contract by phone number
     * @param phone a phone number
     * @throws NotFoundException in case there is no such phone number
     * @return founded contract
     */

    @Transactional
    public Contract getOneByPhone(String phone) {
        Contract contract = contractDao.getOneByPhoneNumber(phone);
        if (contract == null) {
            throw new NotFoundException();
        }
        log.info("Get contract by phone: " + phone);
        return contract;
    }

    /**
     * Change tariff for contract
     * @param contractId contract Id
     * @param tariffId tariff Id
     */

    @Transactional
    public void changeTariff(Long contractId, Long tariffId) {
        Contract contract = contractDao.getOne(contractId);
        Tariff tariff = tariffService.getOne(tariffId);
        contract.setTariff(tariff);
        contract.setOptions(new HashSet<>());
        log.info("Update contract with tariff: " + tariffId);
        contractDao.update(contract);
    }

    /**
     * Change options for contract
     * @param contractId contract Id
     * @param options set of options IDs
     * @throws NotFoundException in case there is no such tariff
     * @throws EmptyDataException in case null  options
     */

    @Transactional
    public void changeOptions(Long contractId, Set<Long> options) {
        Contract contract = contractDao.getOne(contractId);
        if (contract == null) {
            throw new NotFoundException();
        }

        if (options == null) {
            throw new EmptyDataException();
        }

        Set<Option> optionSet = optionService.getAllByIds(options);

        checkOptionSetForInnerIntersection(optionSet);

        contract.setOptions(optionSet);

        log.info("Update contract :" + contract.getId() + " with options: " + options);
        contractDao.update(contract);
    }

    /**
     * Block a contract with the ROLE who made it
     * @param contractId contract Id
     * @param request request to understand WHO blocked the contract, if it was blocked by ADMIN, USER has no access to unblock it
     * @throws NotFoundException in case there is no such contract
     * @throws EmptyDataException in case null  options
     */

    @Transactional
    public void block(Long contractId, HttpServletRequest request) {
        log.info("Trying block/unblock contract: " + contractId + " ...");
        Contract contract = contractDao.getOne(contractId);
        Object userPrincipal = request.getUserPrincipal();
        String deletedByRole = ((UsernamePasswordAuthenticationToken) userPrincipal).getAuthorities().toString();

        if (contract == null) {
            log.info("Contract not found " + contractId);
            throw new NotFoundException();
        }
        if (!contract.getDeleted()) {
            contract.setDeleted(!contract.getDeleted());
            contract.setDeletedBy(deletedByRole);
        } else {
            contract.setDeleted(!contract.getDeleted());
            contract.setDeletedBy("");
        }

        log.info("Block/unblock contract: " + contractId);
        contractDao.update(contract);
    }
}