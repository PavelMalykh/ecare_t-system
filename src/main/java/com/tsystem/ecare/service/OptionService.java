package com.tsystem.ecare.service;

import com.tsystem.ecare.component.MessageSender;
import com.tsystem.ecare.dao.OptionDao;
import com.tsystem.ecare.entity.Option;
import com.tsystem.ecare.exception.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class OptionService {

    private static final Logger log = Logger.getLogger(OptionService.class);

    @Autowired
    private OptionDao optionDao;

    @Autowired
    private MessageSender messageSender;

    /**
     * Get all options
     * @return list of options
     */

    @Transactional
    public List<Option> getAll() {
        return optionDao.getAll();
    }

    /**
     * Get specific option
     * @param id option ID
     * @throws NotFoundException in case there is no option with such ID
     * @return an option
     */

    @Transactional
    public Option getOne(Long id) {
        Option foundedOption = optionDao.getOne(id);
        if (foundedOption == null) {
            throw new NotFoundException();
        }
        log.info("Get option id " + id);
        return foundedOption;
    }

    /**
     * Create an option
     * @param option option
     * @throws AlreadyCreatedException in case option has already been created
     * @return an option ID
     */

    @Transactional
    public Long createOption(Option option) {
        if (optionDao.getOptionByName(option.getName()) != null) {
            throw new AlreadyCreatedException();
        }
        option.setDeleted(false);
        Long id = optionDao.save(option);
        messageSender.sendMessage("update");
        log.info("Create option id " + id);
        return id;
    }

    /**
     * Get all options by IDs
     * @param ids option IDs
     * @return set of options
     */

    @Transactional
    public Set<Option> getAllByIds(Set<Long> ids) {
        return optionDao.getAllByIds(ids);
    }

    /**
     * Create incompatible/obligatory options rule
     * @param optionId main option
     * @param obligatoryOptions set of obligatory options
     * @param incompatibleOptions set of incompatible options
     * @throws NotFoundException in case there is no option with ID
     * @throws IntersectingOptionsException in case if options overlap each other
     */

    @Transactional
    public void createRule(Long optionId, Set<Long> obligatoryOptions, Set<Long> incompatibleOptions) {
        Option option = optionDao.getOne(optionId);
        if (option == null) {
            throw new NotFoundException();
        }
        if (obligatoryOptions.contains(optionId) || incompatibleOptions.contains(optionId) || !Collections.disjoint(obligatoryOptions, incompatibleOptions)) {
            throw new IntersectingOptionsException();
        }

            if (obligatoryOptions.isEmpty()) {
                Set<Option> incompatibleOptionsSet = optionDao.getAllByIds(incompatibleOptions);
                option.setIncompatibleOption(incompatibleOptionsSet);
                saveOption(option);
            } else if (incompatibleOptions.isEmpty()) {
                Set<Option> obligatoryOptionsSet = optionDao.getAllByIds(obligatoryOptions);
                option.setObligatoryOption(obligatoryOptionsSet);
                saveOption(option);
            } else {
                Set<Option> obligatoryOptionsSet = optionDao.getAllByIds(obligatoryOptions);
                Set<Option> incompatibleOptionsSet = optionDao.getAllByIds(incompatibleOptions);

                option.setIncompatibleOption(incompatibleOptionsSet);
                option.setObligatoryOption(obligatoryOptionsSet);
                saveOption(option);
            }
    }

    /**
     * Save an option
     * @param option an option
     */

    public void saveOption(Option option) {
        optionDao.update(option);
        messageSender.sendMessage("update");
        log.info("Update option id " + option.getId());
    }

    /**
     * Delete an option
     * @param id option ID
     * @throws NotFoundException in case there is no option with ID
     */

    @Transactional
    public void deleteOption(Long id) {
        Option option = optionDao.getOne(id);
        if (option == null) {
            throw new NotFoundException();
        }
        option.setDeleted(true);
        optionDao.update(option);
        messageSender.sendMessage("update");
        log.info("Update option id " + id + " set deleted: true");
    }

    /**
     * Update an option
     * @param option an option
     * @throws NotFoundException in case there is no option with ID
     * @throws DuplicateUniqueException in case option has already taken name
     */

    @Transactional
    public void updateOption(Option option) {
        Option foundedOptionById = optionDao.getOne(option.getId());
        if (foundedOptionById == null) {
            throw new NotFoundException();
        }
        Option foundedOptionByName = optionDao.getOptionByName(option.getName());

        if (foundedOptionByName == null || foundedOptionByName == foundedOptionById) {
            foundedOptionById.setIncompatibleOption(foundedOptionById.getIncompatibleOption());
            foundedOptionById.setObligatoryOption(foundedOptionById.getObligatoryOption());
            foundedOptionById.setPrice(option.getPrice());
            foundedOptionById.setConnectionCost(option.getConnectionCost());
            foundedOptionById.setDeleted(false);
            optionDao.update(foundedOptionById);
            messageSender.sendMessage("update");
            log.info("Update option id " + foundedOptionById.getId());
        } else {
            throw new DuplicateUniqueException();
        }
    }

    /**
     * Check if option name is available
     * @param option an option
     * @throws NotFoundException in case there is no option with ID
     * @return true/false
     */

    @Transactional
    public boolean isNameAvailable(Option option) {
        if (option == null) {
            throw new NotFoundException();
        }
        Option foundedOptionById = optionDao.getOne(option.getId());
        Option foundedOptionByName = optionDao.getOptionByName(option.getName());

        return foundedOptionByName == null || foundedOptionById == foundedOptionByName;
    }

    /**
     * Get set of incompatible options for current option
     * @param id an option ID
     * @throws NotFoundException in case there is no option with ID
     * @return set of options
     */

    @Transactional
    public Set<Option> getIncompatibleOptions(Long id) {
        Option option = optionDao.getOne(id);
        if (option == null) {
            throw new NotFoundException();
        }
        Set<Option> resultSet = new LinkedHashSet<>();
        recursiveIncompatibleOptions(option, resultSet);
        return resultSet;
    }

    /**
     * Get set of obligatory options for current option
     * @param id an option ID
     * @throws NotFoundException in case there is no option with ID
     * @return set of options
     */

    @Transactional
    public Set<Option> getObligatoryOptions(Long id) {
        Option option = optionDao.getOne(id);
        if (option == null) {
            throw new NotFoundException();
        }
        Set<Option> resultSet = new LinkedHashSet<>();
        recursiveObligatoryOptions(option, resultSet);
        return resultSet;
    }

    /**
     * Add all incompatible options for the current option by recursive method
     * @param option the main option
     * @param resultSet set of incompatible options
     */

    public void recursiveIncompatibleOptions(Option option, Set<Option> resultSet) {
        if  (resultSet.add(option)) {
            for (Option incompatibleOption: option.getIncompatibleOption()) {
                recursiveIncompatibleOptions(incompatibleOption, resultSet);
            }
        }
    }

    /**
     * Add all obligatory options for the current option by recursive method
     * @param option the main option
     * @param resultSet set of incompatible options
     */

    public void recursiveObligatoryOptions(Option option, Set<Option> resultSet) {
        if (resultSet.add(option)) {
            for (Option obligatoryOption: option.getObligatoryOption()) {
                recursiveObligatoryOptions(obligatoryOption, resultSet);
            }
        }
    }

    /**
     * Check if option set overlap each other
     * @param optionSet set of options
     */

    public static void checkOptionSetForInnerIntersection(Set<Option> optionSet) {
        for (Option option: optionSet) {
            Set<Option> incompatibleOptions = option.getIncompatibleOption();
            Set<Option> obligatoryOptions = option.getObligatoryOption();
            if (doIntersect(incompatibleOptions, obligatoryOptions)) {
                throw new IntersectingOptionsException();
            }
        }
    }

    public static boolean doIntersect(Set<Option> incompatibleSet, Set<Option> obligatorySet) {
        return !Collections.disjoint(incompatibleSet, obligatorySet);
    }
}