package com.tsystem.ecare.service;

import com.tsystem.ecare.dao.UserDAO;
import com.tsystem.ecare.entity.User;
import com.tsystem.ecare.exception.BlockedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static org.springframework.security.core.userdetails.User.builder;
import static org.springframework.security.core.userdetails.User.withUsername;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userDAO.getByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        if (!user.getEnabled()) {
            throw new BlockedUserException();
        }
        return withUsername(user.getEmail()).password(user.getPassword()).authorities(user.getRoles()).build();
    }

    public User getUserEmail(String email) {
        return userDAO.getByEmail(email);
    }
}
