<%--<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>
<%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
<%--    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">--%>

<%--    <title>Title</title>--%>
<%--</head>--%>
<%--<body>--%>

<%--    <h2>Incorrect login or password, please, try again <button type="button" class="btn btn-primary" onclick="window.location='/eecare_war/login'">Back to login page</button></h2>--%>


<%--    <img src="<c:url value = '/img/login_incorrect.jpg' context='/eecare_war'/>" id="icon" alt="User Icon1"  width="100%"/>--%>

<%--</body>--%>
<%--</html>--%>

<%--&lt;%&ndash;--%>
<%--  Created by IntelliJ IDEA.--%>
<%--  User: pavel.malykh--%>
<%--  Date: 29/09/2020--%>
<%--  Time: 14:17--%>
<%--  To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>
<%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--%>

<%--<html>--%>
<%--<head>--%>
<%--    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">--%>

<%--    <h1>--%>
<%--        Sorry, entity has already been created, please, try another.--%>
<%--    </h1>--%>

<%--    <img src="<c:url value = '/img/conflict.jpeg' context='/eecare_war'/>" id="icon" alt="User Icon1" width="100%"/>--%>

<%--    <button type="button" class="btn btn-primary" onclick="window.history.back()">Ok</button>--%>

<%--</head>--%>
<%--<body>--%>

<%--</body>--%>
<%--</html>--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
<link rel="stylesheet" href="<c:url value='/styles/backgroun-incorrect-login.css' context='/eecare_war'/>">

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body style="background-color: #343434;">
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.history.back()" style="color: wheat; margin-left: 10%;">Back</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Incorrect login or password, please, try again
    </span>

</nav>

<div class="background" style="height: 750px;margin-left: 600px;"></div>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

</body>
</html>
