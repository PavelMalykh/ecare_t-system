<!doctype html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm"%>

<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
<link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">


<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<script src="<c:url value='/scripts/deleteOption.js' context='/eecare_war'/>"></script>

<c:set var="option" value="${option}" scope="session"></c:set>

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <style>
        span.error {
            margin-left: 150px;
            font-family: 'Chilanka', cursive;
            color: palevioletred;
            display: inline-block;
        }
    </style>
    <title>Title</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/option'" style="color: wheat; margin-left: 10%;">Back to options page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Option: ${option.getName()}
    </span>
    <c:if test="${!option.getDeleted()}">
        <button type="button" id="create" class="button-2" onclick="deleteOption()" style="color: wheat">Delete</button>
    </c:if>
</nav>





<frm:form method="POST" action="${pageContext.request.contextPath}/option/update" modelAttribute="option" cssClass="form-style-4" cssStyle="height: 1000px;margin-top: -100px;">
    <frm:input path="id" type="hidden" class="form-control" id="option-id" aria-describedby="OptionHelp" placeholder="Enter option name"/>

    <label class="label-input">
        <span class="span-input">Name</span>
        <frm:input cssClass="span-input" path="name" type="text" id="optionName" aria-describedby="TariffHelp"/>
        <br>
        <br>
        <frm:errors path="name" cssClass="error"/>
        <c:if test="${not empty nameEmptyError}">
            <label style="margin-left: 150px;font-family: 'Chilanka', cursive;color: palevioletred;display: inline-block;" >${nameEmptyError}</label>
        </c:if>
    </label>
    <label class="label-input">
        <span class="span-input">Price</span>
        <frm:input cssClass="span-input" path="price" type="number" id="optionPrice" placeholder="max 500"/>
        <br>
        <br>
        <frm:errors path="price" cssClass="error"/>
    </label>
    <label class="label-input">
        <span class="span-input">Connection cost</span>
        <frm:input cssClass="span-input" path="connectionCost" type="number" step="any" id="optionConnectionPrice" placeholder="max 300"/>
        <br>
        <br>
        <frm:errors path="connectionCost" cssClass="error" cssStyle="margin-left: 0px"/>
    </label>
    <div class="container" style="padding-bottom: 60px;">
        <div class="row">
            <div class="col-sm" style="padding-left: 400px;">
                <div style="font-size: 40px;font-family: cursive;padding-bottom: 10px;"> Obligatory options </div>
                <div style="padding-top: 20px;font-family: 'Chilanka', cursive;font-size: 25px;padding-bottom: 40px;color: wheat;">
                    <c:if test="${!option.getObligatoryOption().isEmpty()}">
                        <c:forEach items="${option.getObligatoryOption()}" var = "option">
                            <ul>
                                <li>${option.getName()}</li>
                            </ul>
                        </c:forEach>
                    </c:if>
                    <c:if test="${option.getObligatoryOption() == null || option.getObligatoryOption().isEmpty()}">
                        No options were included
                    </c:if>
                </div>
            </div>
            <div class="col-sm">
                <div style="font-size: 40px;font-family: cursive;padding-bottom: 10px;"> Incompatible options </div>
                <div style="padding-top: 20px;font-family: 'Chilanka', cursive;font-size: 25px;padding-bottom: 40px;color: wheat;">
                    <c:if test="${!option.getIncompatibleOption().isEmpty()}">
                        <c:forEach items="${option.getIncompatibleOption()}" var = "option">
                            <ul>
                                <li>${option.getName()}</li>
                            </ul>
                        </c:forEach>
                    </c:if>
                    <c:if test="${option.getIncompatibleOption() == null || option.getIncompatibleOption().isEmpty()}">
                        No options were included
                    </c:if>
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="margin-left: 60px;">
        <div class="row">
            <div class="col-sm" style="padding-left: 400px;">
                <button type="submit" class="button-2" style="margin-left: 50%;height: 78px;" >Update</button>
            </div>
            <c:if test="${!option.getDeleted()}">
            <div class="col-sm" style="padding-left: 80px;">
                <button type="button" id="change-rule" class="button-2" onclick="window.location='/eecare_war/option/rule/obligatory'">Obligatory options</button>
            </div>
            <div class="col-sm" style="padding-left: 30px;">
                <button type="button" id="change-rule-штсщьзфешиду" class="button-2" onclick="window.location='/eecare_war/option/rule/incompatible'">Incompatible options</button>
            </div>
            </c:if>
        </div>
    </div>

</frm:form>

<div class="modal" tabindex="-1" role="dialog" id="success">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Option was successfully deleted</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/option/${option.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/option/${option.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>


<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>


<%-- <h3> Obligatory Options </h3>--%>
<%-- <select class="selectpicker" multiple data-live-search="true" id="obligatory-options">--%>
<%--        <c:forEach items="${option.getObligatoryOption()}" var="option">--%>
<%--            <option value="${option.getId()}" selected disabled>${option.getName()}</option>--%>
<%--        </c:forEach>--%>
<%--</select>--%>

<%-- <h3> Incompatible Options </h3>--%>
<%-- <select class="selectpicker" multiple data-live-search="true" id="incompatible-options">--%>
<%--        <c:forEach items="${option.getIncompatibleOption()}" var="option">--%>
<%--            <option value="${option.getId()}" selected disabled>${option.getName()}</option>--%>
<%--        </c:forEach>--%>
<%--</select>--%>

    <c:set var="myOption" value="${option}" scope="session"></c:set>

    <c:set var="myIncompatibleOptions" value="${option.getIncompatibleOption()}" scope="session"></c:set>
    <c:set var="myObligatoryOptions" value="${option.getObligatoryOption()}" scope="session"></c:set>

</body>
</html>
