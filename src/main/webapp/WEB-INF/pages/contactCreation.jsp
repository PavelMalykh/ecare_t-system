<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<%--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">--%>
<%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />--%>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--%>
<%--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>--%>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm"%>

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">


<%--    <script src="https://code.jquery.com/jquery-1.10.2.js"--%>
<%--            type="text/javascript"></script>--%>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script src="<c:url value='/scripts/phoneMask.js' context='/eecare_war'/>"></script>

    <title>Title</title>

    <script src="<c:url value='/scripts/contractCreation.js' context='/eecare_war'/>"></script>
    <script src="<c:url value='/scripts/optionsSet.js' context='/eecare_war'/>"></script>

    <title>Title</title>
</head>
<body>

<div class="container-fluid" style="position: absolute;">
    <div class="row">
        <div class="col-sm">
            <h1> Select customer </h1>
            <select class="selectpicker" data-live-search="true" name="customer" id="customer" onchange="showTariffAndPhone()">
                <option value=""></option>
                <c:forEach items="${customers}" var="customer">
                    <c:if test="${customer.getUser().getEnabled()}">
                        <option value="${customer.getId()}">${customer.getFirstName()}</option>
                    </c:if>
                </c:forEach>
            </select>
        </div>
        <div class="col-sm" id="tariff-block" style="display: none">
            <h1> Select Tariff </h1>
            <select class="selectpicker" data-live-search="true" name="tariff" id="tariff" onchange="getOptions()">
                <option value=""></option>
                <c:forEach items="${tariffs}" var="tariff">
                    <c:if test="${!tariff.getDeleted()}">
                        <option value="${tariff.getId()}">${tariff.getName()}</option>
                    </c:if>
                </c:forEach>
            </select>
        </div>
        <div class="col-sm">
            <div id="options-list-block" style="display: none">
                <h1> Available options </h1>
                <select name="option" multiple id="options-list" onchange="getOptionSet()">
                    <c:forEach items="${tariff.getOptionSet()}" var="option">
                        <c:if test="${!option.getDeleted()}">
                            <option value="${option.getId()}">${option.getName()}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
            <div id="incompatible-list-block" style="display: none">
                <h1> Incompatible options </h1>
                <select name="option" multiple id="incompatible-list">
                    <c:forEach items="${tariff.getOptionSet()}" var="option">
                        <c:if test="${!option.getDeleted()}">
                            <option value="${option.getId()}">${option.getName()}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
            <div id="obligatory-list-block" style="display: none">
                <h1> Obligatory options </h1>
                <select name="option" multiple id="obligatory-list">
                    <c:forEach items="${tariff.getOptionSet()}" var="option">
                        <c:if test="${!option.getDeleted()}">
                            <option value="${option.getId()}">${option.getName()}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="col-sm" id="phone-block" style="display: none">
            <h1> Phone number </h1>
            <input type="text" id="phone" placeholder="Starts with 8-800" value="${phone}" data-id="${isOk}"/>
            <br>
            <br>
            <button type="button" id="check-number" class="btn btn-primary">Check phone number</button>
            <br>
            <br>
            <button type="button" id="generate" class="btn btn-primary">Generate new number</button>
        </div>
        <div class="col-sm" id="submit-block" style="display: none; text-align: center; margin-top: 50px;">
            <button type="button" id="submit" class="btn btn-primary btn-lg">Submit</button>
        </div>
        <div class="col-sm" style="text-align: center; margin-top: 50px;">
            <button type="button" id="cancel" class="btn btn-primary btn-lg" onclick="window.location.href='/eecare_war/contract'">Back to contract page</button>
        </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="success-creation">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Contract was successfully created</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/contract/${contractId}'">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>