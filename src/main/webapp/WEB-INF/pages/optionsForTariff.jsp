<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Title</title>
</head>
<body>

<form class="form-style-4" style="padding-top: 100px;height: 980px;">

    <c:forEach items="${obligatoryOptions}" var="option">
        <c:if test="${!option.getDeleted()}" >
            <div class="accordion" id="accordionExample">
                    <%--                <c:choose>--%>
                    <%--                    <c:when test="">--%>

                    <%--                    </c:when>--%>
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <div class="form-check form-check-inline" style="margin-left: 90%;">
                            <input type="checkbox" name="ObligatoryOption" class="form-check-input" value="${option.getId()}" data-name="${option.getName()}" onchange="setOptions(this)">
                            <label class="form-check-label"></label>
                        </div>
                            <%--                    <button type="button" class="btn btn-secondary" style="margin-left: 90%;">Select</button>--%>
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" option-id="${option.getId()}" >
                            <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                                    ${option.getName()}
                            </span>
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <span class="navbar-text; h3" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                                Price: ${option.getPrice()}
                            </span>
                        </div>
                    </div>
                </div>
                    <%--                </c:choose>--%>
            </div>
        </c:if>
    </c:forEach>

    <c:forEach items="${incompatibleOptions}" var="option">
        <c:if test="${!option.getDeleted()}" >
            <div class="accordion" id="accordionExample">
                    <%--                <c:choose>--%>
                    <%--                    <c:when test="">--%>

                    <%--                    </c:when>--%>
                <div class="card">
                    <div class="card-header" id="headingOne-1">
                        <div class="form-check form-check-inline" style="margin-left: 90%;">
                            <input type="checkbox" name="incompatibleOption" class="form-check-input" value="${option.getId()}" data-name="${option.getName()}" onchange="setOptions(this)">
                            <label class="form-check-label"></label>
                        </div>
                            <%--                    <button type="button" class="btn btn-secondary" style="margin-left: 90%;">Select</button>--%>
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" option-id="${option.getId()}" >
                            <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                                    ${option.getName()}
                            </span>
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne-1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <span class="navbar-text; h3" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                                Price: ${option.getPrice()}
                            </span>
                        </div>
                    </div>
                </div>
                    <%--                </c:choose>--%>
            </div>
        </c:if>
    </c:forEach>
</form>

</body>
</html>
