<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
<link rel="stylesheet" href="<c:url value='/styles/background-forbidden.css' context='/eecare_war'/>">

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body style="background-color: #ffa700;">
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/main'" style="color: wheat; margin-left: 10%;">Back to main menu</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Sorry, your account is blocked, please, contract us (e-care@help.ru)
    </span>

</nav>

<%
%>

<div class="background" style="height: 750px;margin-left: 200px;"></div>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

</body>
</html>
