<%@ page language="java" contentType="text/html; UTF-8" %>
<%@ taglib prefix="auth" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">
    <style>
        span.error {
            margin-left: 150px;
            font-family: 'Chilanka', cursive;
            color: palevioletred;
            display: inline-block;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Guru Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/main'" style="color: wheat; margin-left: 10%;">Back to main page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Registration
    </span>

</nav>


<frm:form method="POST" action="${pageContext.request.contextPath}/customer" modelAttribute="customer" cssClass="form-style-4" cssStyle="margin-top: -140px;height: 1300px;">

    <label class="label-input">
        <span class="span-input">Name*</span>
        <frm:input path="firstName" type="text" class="form-control" id="Name" aria-describedby="OptionHelp" placeholder="max 20 characters"/>
        <br>
        <br>
        <frm:errors path="firstName" cssClass="error" cssStyle="margin-top: -30px;"/>
    </label>
    <label class="label-input">
        <span class="span-input">Lastname*</span>
        <frm:input path="lastName" type="text" class="form-control" id="LastName" placeholder="max 20 characters"/>
        <br>
        <br>
        <frm:errors path="lastName" cssClass="error" cssStyle="margin-top: -30px;"/>
    </label>
    <label class="label-input">
        <span class="span-input">Birthday*</span>
        <frm:input path="birthday" type="date"  class="form-control" id="Birthday" placeholder="Birthday" cssStyle="background-color: wheat;width: 300px;" max="2002-10-01"/>
        <br>
        <br>
        <frm:errors path="birthday" cssClass="error" cssStyle="margin-top: -30px;"/>
    </label>
    <label class="label-input">
        <span class="span-input">Passport data*</span>
        <frm:input path="passportData" type="text" maxlength="10" class="form-control" id="PassportData" placeholder="should be less or equal than 10 characters"/>
        <br>
        <br>
        <frm:errors path="passportData" cssClass="error" cssStyle="margin-top: -30px;"/>
    </label>
    <label class="label-input">
        <span class="span-input">Address*</span>
        <frm:input path="address" type="text" class="form-control" id="Address" placeholder="should be less or equal than 100 characters"/>
        <br>
        <br>
        <frm:errors path="address" cssClass="error" cssStyle="margin-top: -30px;"/>
    </label>
    <label class="label-input">
        <span class="span-input">Email*</span>
        <frm:input path="email" type="text" class="form-control" id="email" placeholder="Email"/>
        <br>
        <br>
        <frm:errors path="email" cssClass="error" cssStyle="margin-top: -30px;"/>
    </label>
    <label class="label-input">
        <span class="span-input">Password*</span>
        <frm:input path="password" type="password" class="form-control" id="password" cssStyle="background-color: wheat;width: 300px;" placeholder="Should be between 6 and 15 characters"/>
        <br>
        <br>
        <frm:errors path="password" cssClass="error" cssStyle="margin-top: -30px;"/>
    </label>
    <div class="container" style="margin-top: 70px;">
        <div class="row">
            <div class="col-sm-6" style="padding-left: 400px;">
                <button type="submit" class="button-2" style="margin-left: 20%;">Submit</button>
            </div>
        </div>
    </div>
</frm:form>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">� 2020 T-Systems</p>
        </div>
</div>
</footer>
</body>
</html>