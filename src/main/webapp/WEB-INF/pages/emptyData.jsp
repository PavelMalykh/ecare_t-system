<%--
  Created by IntelliJ IDEA.
  User: pavel.malykh
  Date: 29/09/2020
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <h1>
        Sorry but empty data was transferred, please provide correct data.
    </h1>

    <img src="<c:url value = 'https://http.cat/400.jpg' context='/eecare_war'/>" id="icon" alt="User Icon1" />

</head>
<body>

</body>
</html>
