<%@ page import="org.springframework.security.authentication.UsernamePasswordAuthenticationToken" %>
<!doctype html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>


<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
<link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">
<script src="<c:url value='/scripts/blockContract.js' context='/eecare_war'/>"></script>
<script src="<c:url value='/scripts/contractCreation.js' context='/eecare_war'/>"></script>


<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.history.back()" style="color: wheat; margin-left: 10%;">Back</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Contract
    </span>

</nav>

<input id="contract-id" value="${contract.getId()}" hidden>


<c:forEach items="${tariffs}" var="tariff">
    <c:if test="${!tariff.getDeleted() && !myTariff.equals(tariff)}">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <button type="button" class="button-3" style="margin-left: 90%;background-color: #353a40" id="tariff-id" value="${tariff.getId()}" tariff-id="${tariff.getId()}" onclick="updateContractTariff(this)">Select</button>
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                        ${tariff.getName()}
                </span>
                        </button>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                <span class="navbar-text; h3" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                    Price: ${tariff.getPrice()}
                </span>
                    </div>
                </div>
            </div>

        </div>

    </c:if>
</c:forEach>

<div class="modal" tabindex="-1" role="dialog" id="success-updated">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Tariff was successfully updated</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/contract/${contract.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/contract/${contract.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
