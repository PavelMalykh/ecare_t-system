<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
<link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">


<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>


<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/main'" style="color: wheat; margin-left: 10%;">Back to main page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Success registration
    </span>

</nav>

<div class="form-style-4" style="padding-top: 80px; height: 800px;">
    <div class="row" style="padding-left: 30%;font-size: 40px;font-family: 'Chilanka', cursive; color: wheat;">
        <p name="customerId" id="id" value="${customerId}" style="padding-bottom: 100px;">Customer with id = ${customerId} was successfully created</p>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-6" style="padding-left: 600px;">
                <button type="button" id="submit" class="button-2" onclick="window.location.href='${pageContext.request.contextPath}/login'">Login page</button>
            </div>
        </div>
    </div>
</div>


<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

</body>
</html>
