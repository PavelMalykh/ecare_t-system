<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>
    <title>Title</title>

    <script src="<c:url value='/scripts/optionRules.js' context='/eecare_war'/>"></script>
    <title>Title</title>
</head>
<body>
    <div>
            <div id="intersecting-error" data-id="true">
                <h1>Sorry, you selected intersecting options </h1>
            </div>

        <button type="button" class="btn btn-primary" onclick="window.history.back()">Ok</button>
    </div>
</body>
</html>
