<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <div>
        <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

        <div id="empty-tariff-exception">
            <h1>
                Sorry, you need to select a tariff.
            </h1>
        </div>
        <img src="<c:url value = '/img/wrong_number.png' context='/eecare_war'/>" id="icon" alt="User Icon1" />
    </div>
</head>
<body>

</body>
</html>
