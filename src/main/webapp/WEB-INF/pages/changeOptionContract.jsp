<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<%--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">--%>
<%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />--%>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--%>
<%--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>--%>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<script src="https://code.jquery.com/jquery-1.10.2.js"--%>
<%--        type="text/javascript"></script>--%>
<title>Title</title>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">

<%--    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">--%>
<%--    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">--%>
    <script src="<c:url value='/scripts/contractCreation.js' context='/eecare_war'/>"></script>
    <script src="<c:url value='/scripts/contractOptions.js' context='/eecare_war'/>"></script>
    <script src="<c:url value='/scripts/updateOptionsForContract.js' context='/eecare_war'/>"></script>


    <title>Title</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.history.back()" style="color: wheat; margin-left: 10%;">Back</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Options for contract
    </span>
<%--    <button type="button" id="save" class="btn btn-primary" onclick="updateContract()">Save</button>--%>

    <button type="button" id="save" class="button-2" onclick="updateOptionsForContract()">Save</button>
</nav>

<%--<h1>Contract</h1>--%>
<%--<div class="form-group">--%>
<%--    <label>Id</label>--%>
    <input type="text" class="form-control" id="contract-id" value="${contractId}" readonly hidden>
<%--</div>--%>
<%--<h2>Tariff</h2>--%>
<%--<div class="form-group">--%>
<%--    <label>Id</label>--%>
    <input type="text" class="form-control" id="tariff-id" value="${tariff.getId()}" readonly hidden>
<%--    <label>Name</label>--%>
    <input type="text" class="form-control" id="tariff-name" value="${tariff.getName()}" readonly hidden>
<%--</div>--%>
<%--<h1> Available options</h1>--%>
<%--<div class="form-group" id="options-list-block">--%>
<%--    <select class="selectpicker" multiple data-live-search="true" id="options-list">--%>
<%--        <c:forEach items="${tariff.getOptionSet()}" var="option">--%>
<%--            <c:if test="${!option.getDeleted()}">--%>
<%--                <c:choose>--%>
<%--                    <c:when test="${contract.getOptions().contains(option)}">--%>
<%--                        <option value="${option.getId()}" selected>${option.getName()}</option>--%>
<%--                    </c:when>--%>
<%--                    <c:otherwise>--%>
<%--                        <option value="${option.getId()}" >${option.getName()}</option>--%>
<%--                    </c:otherwise>--%>
<%--                </c:choose>--%>
<%--            </c:if>--%>
<%--        </c:forEach>--%>
<%--    </select>--%>
<%--</div>--%>

<%--${obligatoryOptionsTariff}--%>
<%--${incompatibleOptionsTariff}--%>

<form style="height: 980px;">

<%--    <input type="text" class="form-control" id="tariff-id" value="${tariff.getId()}" hidden>--%>
<%--    <input type="text" class="form-control" id="tariff-name" value="${tariff.getName()}" hidden>--%>


    <%--    <label class="label-input">--%>
    <%--        <span class="span-input">Id</span>--%>
    <%--        <input type="text" class="form-control" id="tariff-id" aria-describedby="emailHelp" value="${tariff.getId()}"readonly>--%>
    <%--        <br>--%>
    <%--        <br>--%>
    <%--    </label>--%>


    <c:forEach items="${optionSet}" var="option">
        <c:if test="${!option.getDeleted()}" >
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" >
                        <div class="form-check form-check-inline" style="margin-left: 90%;">
                            <c:choose>
                                <c:when test="${options.contains(option)}">
                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" name="tariffOption" class="form-check-input" value="${option.getId()}" onchange="getOptionsAndSetCard(this)" checked>
                                </c:when>
<%--                                <c:when test="${obligatoryOptionsTariff.equals(option)}">--%>
<%--                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" name="tariffOption" class="form-check-input" value="${option.getId()}" onchange="setOptions(this)" checked disabled>--%>
<%--                                </c:when>--%>
<%--                                <c:when test="${incompatibleOptionsTariff.equals(option)}">--%>
<%--                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" name="tariffOption" class="form-check-input" value="${option.getId()}" onchange="setOptions(this)" disabled>--%>
<%--                                </c:when>--%>
                                <c:otherwise>
                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" name="tariffOption" class="form-check-input" value="${option.getId()}" onchange="getOptionsAndSetCard(this)">
                                </c:otherwise>
                            </c:choose>

<%--            <c:when test="${options.contains(option)}">--%>
<%--                <option value="${option.getId()}" selected>${option.getName()}</option>--%>
<%--            </c:when>--%>
<%--            <c:when test="${obligatoryOptionsTariff.equals(option)}">--%>
<%--                <option value="${option.getId()}" selected disabled>${option.getName()}</option>--%>
<%--            </c:when>--%>
<%--            <c:when test="${incompatibleOptionsTariff.equals(option)}">--%>
<%--                <option value="${option.getId()}" disabled>${option.getName()}</option>--%>
<%--            </c:when>--%>
<%--            <c:otherwise>--%>
<%--                <option value="${option.getId()}" >${option.getName()}</option>--%>
<%--            </c:otherwise>--%>
<%--        </c:choose>--%>

                            <label class="form-check-label"></label>
                        </div>
                            <%--                    <button type="button" class="btn btn-secondary" style="margin-left: 90%;">Select</button>--%>
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" option-id="${option.getId()}" >
                        <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                                ${option.getName()}
                        </span>
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                        <div class="card-body">
                        <span class="navbar-text; h3" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                            Price: ${option.getPrice()}
                        </span>
                        </div>
                    </div>
                </div>
                    <%--                </c:choose>--%>
            </div>
        </c:if>
    </c:forEach>

    <%--    <c:choose>--%>
    <%--        <c:when test="${optionSet.contains(option)}">--%>
    <%--            <option value="${option.getId()}" selected>${option.getName()}</option>--%>
    <%--        </c:when>--%>
    <%--        <c:when test="${obligatoryOptionsTariff.equals(option.getName())}">--%>
    <%--            <option value="${option.getId()}" selected disabled>${option.getName()}</option>--%>
    <%--        </c:when>--%>
    <%--        <c:when test="${incompatibleOptionsTariff.equals(option.getName())}">--%>
    <%--            <option value="${option.getId()}" disabled>${option.getName()}</option>--%>
    <%--        </c:when>--%>
    <%--        <c:otherwise>--%>
    <%--            <option value="${option.getId()}" >${option.getName()}</option>--%>
    <%--        </c:otherwise>--%>
    <%--    </c:choose>--%>


</form>


<%--<div class="container-fluid">--%>
<%--    <div class="row">--%>
<%--        <div class="col-sm">--%>

<%--            <select class="selectpicker" multiple data-live-search="true" id="options-list">--%>
<%--                <c:forEach items="${optionSet}" var="option">--%>
<%--                    <c:if test="${!option.getDeleted()}">--%>
<%--                        <c:choose>--%>
<%--                            <c:when test="${options.contains(option)}">--%>
<%--                                <option value="${option.getId()}" selected>${option.getName()}</option>--%>
<%--                            </c:when>--%>
<%--                            <c:when test="${obligatoryOptionsTariff.equals(option)}">--%>
<%--                                <option value="${option.getId()}" selected disabled>${option.getName()}</option>--%>
<%--                            </c:when>--%>
<%--                            <c:when test="${incompatibleOptionsTariff.equals(option)}">--%>
<%--                                <option value="${option.getId()}" disabled>${option.getName()}</option>--%>
<%--                            </c:when>--%>
<%--                            <c:otherwise>--%>
<%--                                <option value="${option.getId()}" >${option.getName()}</option>--%>
<%--                            </c:otherwise>--%>
<%--                        </c:choose>--%>
<%--                    </c:if>--%>
<%--                </c:forEach>--%>
<%--            </select>--%>
<%--        </div>--%>

<%--</div>--%>
<%--</div>--%>

<div class="modal" tabindex="-1" role="dialog" id="success">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Options were successfully updated</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/contract/${contract.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/contract/${contract.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

<%--<br>--%>
<%--<button type="button" id="save" class="btn btn-primary" onclick="updateContract()">Save</button>--%>
<%--<br>--%>
<%--<br>--%>
<%--<button type="button" id="cancel" class="btn btn-primary" onclick="window.history.back()">Cancel</button>--%>

</body>
</html>
