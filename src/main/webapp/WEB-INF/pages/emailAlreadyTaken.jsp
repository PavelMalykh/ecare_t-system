<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>
<div>

        <div id="already-taken-email"> Sorry, email already taken, please, try another one </div>

        <button type="button" class="btn btn-primary"  onclick="window.history.back()">Back</button>




</div>

<div>
    <div class="modal" tabindex="-1" role="dialog" id="fail-update-phone">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: wheat;">
                    <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Sorry, phone already been taken, please, try another one</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/customer/${customer.getId()}'">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-footer" style="background-color: black;">
                    <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/customer/${customer.getId()}'">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>