<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<%--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--%>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>--%>
<%--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>--%>
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<script src="https://code.jquery.com/jquery-1.10.2.js"--%>
<%--        type="text/javascript"></script>--%>

<%--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">--%>
<%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />--%>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--%>
<%--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>--%>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>--%>
<html>
<head>

    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<%--    <script src="https://code.jquery.com/jquery-3.5.1.js"--%>
<%--            type="text/javascript"></script>--%>
<%--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />--%>

<%--&lt;%&ndash;    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>&ndash;%&gt;--%>
<%--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>--%>
<%--&lt;%&ndash;    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>&ndash;%&gt;--%>
<%--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--%>

    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <script src="<c:url value='/scripts/creationWizard.js' context='/eecare_war'/>"></script>
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">

    <script src="<c:url value='/scripts/phoneMask.js' context='/eecare_war'/>"></script>
    <script src="<c:url value='/scripts/contractCreation.js' context='/eecare_war'/>"></script>
    <script src="<c:url value='/scripts/optionsSet.js' context='/eecare_war'/>"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <title>Title</title>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/adminPage'" style="color: wheat; margin-left: 10%;">Back to admins page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat;">
        Select phone
    </span>

    <p style="padding-top: 20px; margin-left: 200px;">
        <button class="button-2" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Shopping cart
        </button>
    </p>
    <div class="collapse" id="collapseExample" style="padding-left: 20px;">
        <div class="card card-body" style="background-color: wheat;height: max-content;width: max-content;padding-left: 10px;font-family: 'Chilanka', cursive;">
            - Tariff: ${selectedTariff.getName()} | Price: ${selectedTariff.getPrice()}
            <br>
            <div id="optionsBlock">
                <c:if test="${selectedOptions == null || selectedOptions.isEmpty()}">
                    - No options were selected
                </c:if>
                <c:if test="${selectedOptions != null && !selectedOptions.isEmpty()}" >
                    - Options:
                    <ul>
                        <c:forEach items="${selectedOptions}" var="option">
                            <li>
                                    ${option.getName()}
                            </li>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
        </div>
    </div>
<%--    <div class="collapse" id="collapseExample" style="padding-left: 20px;">--%>
<%--        <div class="card card-body" style="background-color: wheat;height: 185px;width: 280px;padding-left: 10px;font-family: 'Chilanka', cursive;">--%>
<%--            - Tariff: ${selectedTariff.getName()} | Price: ${selectedTariff.getPrice()}--%>
<%--            <br>--%>
<%--            <div id="optionsBlock">--%>
<%--                <c:if test="${selectedOptions == null || selectedOptions.isEmpty()}">--%>
<%--                    - No options were selected--%>
<%--                </c:if>--%>
<%--                <c:if test="${selectedOptions != null && !selectedOptions.isEmpty()}">--%>
<%--                    - Options:--%>
<%--                    <ul>--%>
<%--                        <c:forEach items="${selectedOptions}" var="option">--%>
<%--                            <li>--%>
<%--                                    ${option.getName()}--%>
<%--                            </li>--%>
<%--                        </c:forEach>--%>
<%--                    </ul>--%>
<%--                </c:if>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>

</nav>

<%--tariff: ${tariff}--%>
<%--customer: ${customer}--%>
<%--phone: ${phone}--%>
<%--options: ${selectedOptions}--%>


<br>
<div class="container" style="padding-top: 1px;">
    <div class="row">
        <div class="col-sm" style="margin-top: 10px;">
            <button class="button-1" onclick="window.history.back()">Back</button>
        </div>
        <div class="col-sm-9">
            <div class="progress" style="margin-top: 3%; margin-bottom: 65px;">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
            </div>
        </div>
        <div class="col-sm-1" style="margin-top: 10px;">
            <button class="button-1" id="submit">Create</button>
        </div>
    </div>

</div>

<div class="container">
    <div class="row">
        <div class="col-sm" id="phone-block">
            <h1 style="margin-left: 400px; font-family: 'Chilanka', cursive;"> Phone number </h1>
            <input type="text" id="phone" style="margin-left: 440px; font-family: 'Chilanka', cursive;height: 50px;width: 270px;font-size: 30px;" placeholder="Starts with 8-800"  data-id="${isOk}"/>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-sm" style="margin-left: 300px;">
                        <button type="button-1" id="check-number" class="button-1">Check phone number</button>
                    </div>
                    <div class="col-sm" style="margin-right: 290px;">
                        <button type="button" id="generate" class="button-1">Generate new number</button>
                    </div>
                </div>
            </div>
        </div>
<%--        <div class="col-sm" id="submit-block" style="text-align: center; margin-top: 50px;">--%>
<%--            <button type="button" id="submit" class="btn btn-primary btn-lg">Submit</button>--%>
<%--        </div>--%>

    </div>
</div>





<input type="text" id="customer" value="${customer.getId()}" hidden/>
<input type="text" id="tariff" value="${tariff.getId()}" hidden/>
    <c:forEach items="${selectedOptions}" var="option">
        <input type="text" class="selected-options" value="${option.getId()}" hidden/>

<%--        <c:set var="options" value="${option.getId()}" scope="session"></c:set>--%>
    </c:forEach>

<%--<input type="text" id="options" value="${selectedOptions}" hidden/>--%>

<div class="modal" tabindex="-1" role="dialog" id="empty-number-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Sorry, you need to input or generate a number.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="already-taken-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Sorry, phone has already been taken, please, try another one</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="ok-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">It's ok to take this number</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="incorrect-format-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Incorrect format number, please try another (example: 8-800-555-35-35)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="deleted-tariff">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                    <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Sorry, tariff was deleted, please select another one</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" >Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="deleted-option">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Sorry, one or several options were deleted, please, select other options</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" >Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="success-tariff">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Tariff was successfully changed</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/contract/${contract.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/contract/${contract.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="success-creation">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Contract was successfully created</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/contract'">Close</button>
            </div>
        </div>
    </div>
</div>
<%--<div class="col-sm" id="phone-block" style="display: none">--%>
<%--    <h1> Phone number </h1>--%>
<%--    <input type="text" id="phone" placeholder="Starts with 8-800" value="${phone}" data-id="${isOk}"/>--%>

<%--&lt;%&ndash;    <input type="text" id="phone" placeholder="Starts with 8-800" value="${phone}" data-id="${isOk}"/>&ndash;%&gt;--%>
<%--    <br>--%>
<%--    <br>--%>
<%--    <button type="button" id="check-number" class="btn btn-primary">Check phone number</button>--%>
<%--    <br>--%>
<%--    <br>--%>
<%--    <button type="button" id="generate" class="btn btn-primary">Generate new number</button>--%>
<%--</div>--%>
<%--<div class="col-sm" id="submit-block" style="display: none; text-align: center; margin-top: 50px;">--%>
<%--    <button type="button" id="submit" class="btn btn-primary btn-lg">Submit</button>--%>
<%--</div>--%>

<%--<c:forEach items="${tariffs}" var="tariff">--%>
<%--    --%>
<c:set var="selectedOptions" value="${selectedOptions}" scope="session"></c:set>


<%--</c:forEach>--%>

</body>
</html>
