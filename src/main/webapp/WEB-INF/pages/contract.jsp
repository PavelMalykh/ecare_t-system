<%@ page import="org.springframework.security.authentication.UsernamePasswordAuthenticationToken" %>
<!doctype html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>


<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
<link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">
<script src="<c:url value='/scripts/blockContract.js' context='/eecare_war'/>"></script>

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>

<%
    Object userPrincipal = request.getUserPrincipal();
    Boolean isAdmin = ((UsernamePasswordAuthenticationToken) userPrincipal).getAuthorities().toString().equals("[ADMIN]");
    request.setAttribute("isAdmin", isAdmin);
%>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.history.back()" style="color: wheat; margin-left: 10%;">Back</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Contract
    </span>

    <c:if test="${isAdmin || !contract.getDeletedBy().equals('[ADMIN]')}">
        <c:if test="${!contract.getDeleted()}">
            <button type="button" class="button-2" onclick="blockContract()">Block</button>
        </c:if>
        <c:if test="${contract.getDeleted()}">
            <button type="button" class="button-2" onclick="blockContract()">Unblock</button>
        </c:if>
    </c:if>
</nav>

<form class="form-style-4" style="margin-top: -150px;height: 1600px;">
<%--    <label class="label-input">--%>
<%--        <span class="span-input">Id</span>--%>
        <input type="text" class="span-input" id="cotract-id" aria-describedby="emailHelp" value="${contract.getId()}" readonly hidden>
<%--    </label>--%>
    <label class="label-input">
        <span class="span-input">Phone</span>
        <input type="text" class="span-input" id="phone" aria-describedby="emailHelp" value="${contract.getPhone()}" readonly>
    </label>
    <label class="label-input">
        <span class="span-input">Deleted</span>
        <input type="text" class="span-input" id="active" aria-describedby="emailHelp" value="${contract.getDeleted()}" readonly>
    </label>
    <div style="padding-left: 575px;font-size: 40px;font-family: cursive;padding-bottom: 50px;"> Customer </div>
    <label class="label-input">
        <span class="span-input">Firstname</span>
        <input type="text" class="span-input" id="customer-name" aria-describedby="emailHelp" value="${contract.getCustomer().getFirstName()}" readonly>
    </label>
    <label class="label-input">
        <span class="span-input">Lastname</span>
        <input type="text" class="span-input" id="customer-surname" aria-describedby="emailHelp" value="${contract.getCustomer().getLastName()}" readonly>
    </label>
    <label class="label-input">
        <span class="span-input">Birthday</span>
        <input type="text" class="span-input" id="customer-birthday" aria-describedby="emailHelp" value="${contract.getCustomer().getBirthday()}" readonly>
    </label>
    <label class="label-input">
        <span class="span-input">Passsport</span>
        <input type="text" class="span-input" id="customer-passport" aria-describedby="emailHelp" value="${contract.getCustomer().getPassportData()}" readonly>
    </label>
    <label class="label-input">
        <span class="span-input">Address</span>
        <input type="text" class="span-input" id="customer-address" aria-describedby="emailHelp" value="${contract.getCustomer().getAddress()}" readonly>
    </label>
    <div style="padding-left: 575px;font-size: 40px;font-family: cursive;padding-bottom: 50px;"> Tariff </div>
    <label class="label-input">
        <span class="span-input">Id</span>
        <input type="text" class="span-input" id="tariff-id" aria-describedby="emailHelp" value="${contract.getTariff().getId()}" readonly>
    </label>
    <label class="label-input">
        <span class="span-input">Name</span>
        <input type="text" class="span-input" id="tariff-name" aria-describedby="emailHelp" value="${contract.getTariff().getName()}" readonly>
    </label>
    <label class="label-input">
        <span class="span-input">Price</span>
        <input type="text" class="span-input" id="tariff-price" aria-describedby="emailHelp" value="${contract.getTariff().getPrice()}" readonly>
    </label>
    <div style="padding-left: 575px;font-size: 40px;font-family: cursive;padding-bottom: 30px;"> Included options </div>
    <div style="padding-left: 600px;font-family: 'Chilanka', cursive;font-size: 25px;padding-bottom: 40px;color: wheat;">
        <c:if test="${!contract.getOptions().isEmpty()}">
        <c:forEach items="${contract.getOptions()}" var = "contractItem">
            <ul>
                <li>${contractItem.getName()}</li>
            </ul>
        </c:forEach>
        </c:if>
        <c:if test="${contract.getOptions().isEmpty()}">
            No options were included
        </c:if>
    </div>

<c:if test="${isAdmin || !contract.getDeletedBy().equals('[ADMIN]')}">
    <c:if test="${!contract.getDeleted()}">
    <div class="container">
        <div class="row">
            <div class="col-sm-7" style="padding-left: 370px;">
                <button type="button" class="button-2" style="margin-left: 20%;" onclick="window.location='/eecare_war/contract/changeTariff'">Change tariff</button>
            </div>
            <div class="col-sm" style="padding-left: 70px;">
                <button type="button" id="change-rule" class="button-2" onclick="window.location='/eecare_war/contract/changeOption'">Change options</button>
            </div>
        </div>
    </div>
    </c:if>
</c:if>

    <c:set var="contractId" value="${id}" scope="session"></c:set>
<%--    <c:set var="tariff" value="${contract.getTariff()}" scope="session"></c:set>--%>
    <c:set var="contract" value="${contract}" scope="session"></c:set>
    <c:set var="myTariff" value="${contract.getTariff()}" scope="session"></c:set>
    <c:set var="optionSet" value="${myTariff.getOptionSet()}" scope="session"></c:set>
    <c:set var="options" value="${contract.getOptions()}" scope="session"></c:set>




<%--    <c:forEach items="${myTariff.getOptionSet()}" var="option">--%>
<%--        <c:forEach items="${option.getIncompatibleOption()}" var="incompatibleOption">--%>
<%--            <c:set var="incompatibleOptionsTariff" value="${incompatibleOption}" scope="session"></c:set>--%>
<%--        </c:forEach>--%>
<%--    </c:forEach>--%>

<%--    <c:forEach items="${myTariff.getOptionSet()}" var="option">--%>
<%--        <c:forEach items="${option.getObligatoryOption()}" var="obligatoryOption">--%>
<%--            <c:set var="obligatoryOptionsTariff" value="${obligatoryOption.getName()}" scope="session"></c:set>--%>
<%--        </c:forEach>--%>
<%--    </c:forEach>--%>
</form>

<div class="modal" tabindex="-1" role="dialog" id="success">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <c:if test="${contract.getDeleted()}">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Contract was successfully unblocked</h5>
                </c:if>
                <c:if test="${!contract.getDeleted()}">
                    <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Contract was successfully blocked</h5>
                </c:if>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/contract/${contract.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/contract/${contract.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>
</body>
</html>
