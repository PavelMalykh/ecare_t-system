<%--
  Created by IntelliJ IDEA.
  User: pavel.malykh
  Date: 02/10/2020
  Time: 14:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm"%>

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>
    <title>Title</title>

    <script src="<c:url value='/scripts/tariffLinking.js' context='/eecare_war'/>"></script>

</head>
<body>

<h1>
    <form action="list">
        Available tariffs
        <select name="tariff">
            <c:forEach items="${tariffs}" var="tariff">
                <option value="${tariff.getName()}">${tariff.getName()}</option>
            </c:forEach>
        </select>
    </form>

    <h1>
        <form action="list">
            Available options
            <select name="tariff" multiple="multiple" onchange="this.value" id="options">
                <c:forEach items="${options}" var="option">
                    <option value="${option.getName()}">${option.getName()}</option>
                </c:forEach>
            </select>
        </form>
    </h1>
</h1>

<frm:form method="POST" action="${pageContext.request.contextPath}/tariff/linking">
    <pre>

<%--    <form action="list">--%>
<%--        Available options--%>
<%--        <select name="option[]" multiple="multiple" onchange="this.value" id="options-list">--%>
<%--            <c:forEach items="${options}" var="option">--%>
<%--                <option value="${option.getName()}">${option.getName()}</option>--%>
<%--            </c:forEach>--%>
<%--        </select>--%>
<%--    </form>--%>

    <form action="list">
        Available tariffs
        <select name="tariff" id="tariff-name">
            <c:forEach items="${tariffs}" var="tariff">
                <option value="${tariff.getId()}">${tariff.getName()}</option>
            </c:forEach>
        </select>

        Available options
        <select name="option[]" multiple="multiple" onchange="this.value" id="options-list">
            <c:forEach items="${options}" var="option">
                <option value="${option.getId()}">${option.getName()}</option>
            </c:forEach>
        </select>
    </form>
        <button type="button" id="submit-linking" class="btn btn-primary">Submit</button>
    </pre>
</frm:form>

</body>
</html>


<%--<frm:form method="POST" action="${pageContext.request.contextPath}/option" modelAttribute="option">--%>
<%--    <pre>--%>
<%--        <div class="form-group">--%>
<%--            <label for="optionName">Option name</label>--%>
<%--            <frm:input path="name" type="text" class="form-control" id="optionName" aria-describedby="OptionHelp" placeholder="Enter option name"/>--%>
<%--            <frm:errors path="name" cssClass="error"/>--%>
<%--        </div>--%>
<%--        <div class="form-group">--%>
<%--            <label for="optionPrice">Price</label>--%>
<%--            <frm:input path="price" type="number" class="form-control" id="optionPrice" placeholder="Price"/>--%>
<%--            <frm:errors path="price" cssClass="error"/>--%>
<%--        </div>--%>
<%--        <div class="form-group">--%>
<%--            <label for="optionConnectionPrice">Connection price</label>--%>
<%--            <frm:input path="connectionCost" type="number" class="form-control" id="optionConnectionPrice" placeholder="Connection price"/>--%>
<%--            <frm:errors path="connectionCost" cssClass="error"/>--%>
<%--        </div>--%>

<%--        <button type="submit" class="btn btn-primary">Submit</button>--%>
<%--    </pre>--%>
<%--</frm:form>--%>