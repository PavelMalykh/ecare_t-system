<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/background-admin.css' context='/eecare_war'/>">


    <title>Title</title>
    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>
    <script src="<c:url value='/scripts/searchByPhone.js' context='/eecare_war'/>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

</head>
<body>


<nav class="navbar navbar-expand-sm bg-dark navbar-dark h3" id="admin-panel" style="margin-bottom: 30px;">
    <a class="navbar-brand" style="font-family: 'Chilanka', cursive; color: wheat;font-size: 28px;" >Admin page</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="font-family: 'Chilanka', cursive;padding: 10px 10px;">
        <ul class="navbar-nav mr-auto" >
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/contract">Contracts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/customer">Customers</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/tariff">Tariffs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="options" href="${pageContext.request.contextPath}/option">Options</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" id="search-phone" data-mask="00/00/0000" style="padding: 13px;background-color: wheat;">
            <button class="button-2" type="button" id="search-by-phone">Search by phone number</button>
        </form>
        <li class="nav-item">
            <button class="button-2" type="button" id="logout" onclick="window.location.href='${pageContext.request.contextPath}/main'">Logout</button>
        </li>
    </div>
</nav>


<div class="background" style="height: 720px;margin-left: 440px;">

</div>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark" style="margin-top: 40px;">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>
</body>
</html>
