<%--<!doctype html>--%>
<%--<%@ page import="com.tsystem.ecare.dao.UserDAO" %>--%>
<%--<%@ page import="com.tsystem.ecare.entity.User" %>--%>
<%--<%@ page import="java.util.List" %>--%>
<%--<%@ page import="com.tsystem.ecare.controller.CustomerController" %>--%>
<%--<%@ page import="com.tsystem.ecare.dao.TariffDao" %>&lt;%&ndash;--%>
<%--  Created by IntelliJ IDEA.--%>
<%--  User: pavel.malykh--%>
<%--  Date: 11/09/2020--%>
<%--  Time: 16:35--%>
<%--  To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--%>

<%--<html>--%>
<%--<head>--%>
<%--    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">--%>

<%--    <title>Title</title>--%>
<%--</head>--%>
<%--<body>--%>

<%--<%--%>
<%--%>--%>

<%--<h1>--%>
<%--    <div class="alert alert-success" id="option-success-alert" role="alert">--%>
<%--        <h4 class="alert-heading">Success!</h4>--%>
<%--        <p>Option was successfully created</p>--%>
<%--        <hr>--%>
<%--        <p class="mb-0">--%>
<%--            <a href="${pageContext.request.contextPath}/option" id="return">Return to options page</a>--%>
<%--        </p>--%>
<%--    </div>--%>
<%--</h1>--%>


<%--</body>--%>
<%--</html>--%>






<!doctype html>
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
<link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">


<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>


<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/option'" style="color: wheat; margin-left: 10%;">Back to options page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 11%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Option was successfully created
    </span>

<%--    <button type="button" id="create-incompatible" class="button-2" onclick="window.location='${pageContext.request.contextPath}/option/rule/incompatible'" style="color: wheat;margin-right: 180px;">Add incompatible options</button>--%>
<%--    <button type="button" id="create-obligatory" class="button-2" onclick="window.location='${pageContext.request.contextPath}/option/rule/obligatory'" style="color: wheat;margin-right: 180px;">Add obligatory options</button>--%>


</nav>



<c:set var="optionId" value="${option.getId()}" scope="session"></c:set>
<c:set var="myOption" value="${option}" scope="session"></c:set>

<div class="form-style-4" style="padding-top: 80px; height:800px;">
    <div class="row" style="padding-left: 15%;font-size: 40px;font-family: 'Chilanka', cursive; color: wheat;">
        <p name="optionId" id="id" value="${optionId}" style="padding-bottom: 100px;">Option with id = ${optionId} was successfully created. Do you want to add incompatible/obligatory options?</p>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm">
                <button type="button" id="no" class="button-2" onclick="window.location.href='${pageContext.request.contextPath}/option'">No, return to options page</button>
            </div>
            <div class="col-sm">
                <button type="button" id="create-incompatible" class="button-2" onclick="window.location.href='${pageContext.request.contextPath}/option/rule/incompatible'">Add incompatible options</button>
            </div>
            <div class="col-sm">
                <button type="button" class="button-2" onclick="window.location.href='${pageContext.request.contextPath}/option/rule/obligatory'">Add obligatory options</button>
            </div>
        </div>
    </div>
</div>


<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

</body>
</html>