<%--
  Created by IntelliJ IDEA.
  User: pavel.malykh
  Date: 10/09/2020
  Time: 14:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm"%>


<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <style>
        span.error {
            color: red;
            display: inline-block;
        }
    </style>
    <title>Title</title>
</head>
<body>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" crossorigin="anonymous">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" crossorigin="anonymous"></script>

<link rel="stylesheet" href="<c:url value='/styles/login.css' context='/eecare_war'/>">

<%--<link rel="stylesheet" href="<c:url value='/styles/login.css' context='/eecare_war'/>">--%>

<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
    <div id="formContent">
        <!-- Tabs Titles -->

        <!-- Icon -->
        <div class="fadeIn first">
            <img src="<c:url value = '/img/login.png' context='/eecare_war'/>" id="icon" alt="User Icon1" />
        </div>

        <!-- Login Form -->
        <form action="login" method="post">
            <input type="text" id="email" class="fadeIn second" name="username" placeholder="login">
            <input type="text" id="password" class="fadeIn third" name="password" placeholder="password">
            <input type="submit" id="submit" class="fadeIn fourth" value="Log In">
            <input type="button" class="fadeIn fourth" onclick="window.location.href='main'" value="Return to main page">
        </form>

<%--        <form action="login" method="post">--%>
<%--            <frm:input path="username" type="text" id="email" class="fadeIn second" name="username" placeholder="login"/>--%>
<%--            <frm:errors path="username" cssClass="error"/>--%>
<%--            <input type="text" id="password" class="fadeIn third" name="password" placeholder="password">--%>
<%--            <input type="submit" class="fadeIn fourth" value="Log In">--%>
<%--            <input type="button" class="fadeIn fourth" onclick="window.location.href='main'" value="Return to main page">--%>
<%--        </form>--%>

<%--        <frm:input path="price" type="number" step="any" class="form-control" id="tariffPrice" placeholder="Price (max 1000)"/>--%>
<%--        <frm:errors path="price" cssClass="error"/>--%>

        <!-- Remind Passowrd -->
        <div id="formFooter">
            <a class="underlineHover" href="#">Forgot Password?</a>
        </div>

    </div>
</div>
</body>
</html>




<%--&lt;%&ndash;--%>
<%--  Created by IntelliJ IDEA.--%>
<%--  User: pavel.malykh--%>
<%--  Date: 09/09/2020--%>
<%--  Time: 17:15--%>
<%--  To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%--<!doctype html>--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>

<%--<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>
<%--<html>--%>
<%--<head>--%>

<%--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--%>


<%--    <title>WORLD</title>--%>
<%--    <link rel="stylesheet" href="<c:url value='/styles/main.css' context='/eecare_war'/>">--%>

<%--    <script src="<c:url value='/scripts/main.js' context='/eecare_war'/>"></script>--%>
<%--</head>--%>
<%--<body>--%>

<%--<nav class="navbar navbar-expand-lg navbar-dark bg-dark">--%>
<%--    <a class="navbar-brand">--%>
<%--        <img src="<c:url value = '/img/eeecare.png' context='/eecare_war'/>" width="60%" height="20%" alt="no image">--%>
<%--        <a class="logo h1" style="color: orange;">Ecare</a>--%>
<%--    </a>--%>

<%--    <div class="collapse navbar-collapse" id="navbarSupportedContent">--%>
<%--        <ul class="navbar-nav ml-auto">--%>
<%--            <li class="nav-item active h3">--%>
<%--                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>--%>
<%--            </li>--%>
<%--            <li class="nav-item h3">--%>
<%--                <a class="nav-link" href="#">Поддержка</a>--%>
<%--            </li>--%>
<%--            <li class="nav-item dropdown">--%>
<%--                <a class="nav-link dropdown-toggle h3" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--%>
<%--                    Тарифы--%>
<%--                </a>--%>
<%--                <div class="dropdown-menu" aria-labelledby="navbarDropdown">--%>
<%--                    <a class="dropdown-item" href="#">Action</a>--%>
<%--                    <a class="dropdown-item" href="#">Another action</a>--%>
<%--                    <div class="dropdown-divider"></div>--%>
<%--                    <a class="dropdown-item" href="#">Something else here</a>--%>
<%--                </div>--%>
<%--            </li>--%>
<%--        </ul>--%>
<%--        <form class="form-inline my-2 my-lg-0">--%>
<%--            <button type="button" class="btn btn-warning">Личный кабинет</button>--%>
<%--        </form>--%>
<%--    </div>--%>
<%--</nav>--%>

<%--<div class="row">--%>
<%--    <div class="col-4 d-flex justify-content-end">--%>
<%--        <div class="card" style="width: 18rem;">--%>
<%--            <img class="card-img-top" src="<c:url value = '/img/sup.svg' context='/eecare_war'/>" alt="Card image cap">--%>
<%--            <div class="card-body">--%>
<%--                <h5 class="card-title">Тариф 1</h5>--%>
<%--                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--%>
<%--                <a href="#" class="btn btn-primary">Go somewhere</a>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>

<%--    <div class="col-4 d-flex justify-content-center">--%>
<%--        <div class="card" style="width: 18rem;">--%>
<%--            <img class="card-img-top" src="<c:url value = '/img/sup.svg' context='/eecare_war'/>" alt="Card image cap">--%>
<%--            <div class="card-body">--%>
<%--                <h5 class="card-title">Тариф 2</h5>--%>
<%--                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--%>
<%--                <a href="#" class="btn btn-primary">Go somewhere</a>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>

<%--    <div class="col-4 d-flex justify-content-start">--%>
<%--        <div class="card" style="width: 18rem;">--%>
<%--            <img class="card-img-top" src="<c:url value = '/img/sup.svg' context='/eecare_war'/>" alt="Card image cap">--%>
<%--            <div class="card-body">--%>
<%--                <h5 class="card-title">Тариф 3</h5>--%>
<%--                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--%>
<%--                <a href="#" class="btn btn-primary">Go somewhere</a>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</div>--%>

<%--<div class="jumbotron jumbotron-fluid">--%>
<%--    <div class="container">--%>
<%--        <h1 class="display-4">Остались вопросы?</h1>--%>
<%--        <p class="lead">Напишите нам на почту</p>--%>
<%--        <div class="input-group mb-3">--%>
<%--            <input type="text" class="form-control" placeholder="Message" aria-label="Message content" aria-describedby="basic-addon2">--%>
<%--            <div class="input-group-append">--%>
<%--                <button class="btn btn-outline-secondary" type="button" style="color: orange;">Отправить</button>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</div>--%>

<%--<footer class="footer">--%>
<%--    <div class="container-fluid text-center py-3 navbar-dark bg-dark">--%>
<%--        <div class="footer-text">--%>
<%--            <p class="card-text">© 2020 T-Systems</p>--%>
<%--            <a class="footer-link" href="https://gitlab.com/PavelMalykh/ecare_t-system" target="_blank">Gitlab project</a>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</footer>--%>
<%--</body>--%>
<%--</html>--%>
