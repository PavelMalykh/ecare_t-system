<%--
  Created by IntelliJ IDEA.
  User: pavel.malykh
  Date: 09/09/2020
  Time: 17:15
  To change this template use File | Settings | File Templates.
--%>
<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <title>WORLD</title>
    <link rel="stylesheet" href="<c:url value='/styles/main.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">


    <script src="<c:url value='/scripts/main.js' context='/eecare_war'/>"></script>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand">
        <img src="<c:url value = '/img/eeecare.png' context='/eecare_war'/>" width="60%" height="20%" alt="no image">
        <a class="logo h1" style="color: orange;">Ecare</a>
      </a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active h3">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item h3">
          <a class="nav-link" href="#">Support</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle h3" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Tariffs
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <button type="button" class="button-2" style="margin-left: 40px;" onclick="window.location.href='customer/creation'">Registration</button>
        <button type="button" id="login" class="button-2" style="margin-left: 30px;" onclick="window.location.href='login'">Login</button>
      </form>
    </div>
  </nav>

  <div class="row">
    <div class="col-4 d-flex justify-content-end">
      <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="<c:url value = '/img/sup.svg' context='/eecare_war'/>" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Tariff-Business</h5>
          <p class="card-text">All of the options are included in this tariff/</p>
        </div>
      </div>
    </div>

    <div class="col-4 d-flex justify-content-center">
      <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="<c:url value = '/img/sup.svg' context='/eecare_war'/>" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Tariff-Economy</h5>
          <p class="card-text">Pay minimum for only needed options</p>
        </div>
      </div>
    </div>

    <div class="col-4 d-flex justify-content-start">
      <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="<c:url value = '/img/sup.svg' context='/eecare_war'/>" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Tariff - Balanced</h5>
          <p class="card-text">Just all of you need</p>
        </div>
      </div>
    </div>
  </div>

  <div class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1 class="display-4">Any questions?</h1>
      <p class="lead">Feel free to contact us</p>
      <div class="input-group mb-3">
        <input type="text" class="form-control" placeholder="Message" aria-label="Message content" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" style="color: orange;">Send</button>
        </div>
      </div>
    </div>
  </div>

  <footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
      <div class="footer-text">
        <p class="card-text">© 2020 T-Systems</p>
        <a class="footer-link" href="https://gitlab.com/PavelMalykh/ecare_t-system" target="_blank">Gitlab project</a>
      </div>
    </div>
  </footer>
  </body>
</html>
