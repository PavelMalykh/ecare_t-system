<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <style>
        span.error {
            margin-left: 150px;
            font-family: 'Chilanka', cursive;
            color: palevioletred;
            display: inline-block;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Guru Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/option'" style="color: wheat; margin-left: 10%;">Back to options page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Option creation
    </span>

</nav>

<frm:form method="POST" action="${pageContext.request.contextPath}/option" modelAttribute="option" cssClass="form-style-4">

        <label class="label-input">
            <span class="span-input">Name</span>
            <frm:input path="name" type="text" cssClass="span-input" id="optionName" aria-describedby="OptionHelp"/>
            <br>
            <br>
            <frm:errors path="name" cssClass="error"/>
        </label>
        <label class="label-input">
            <span class="span-input">Price</span>
            <frm:input path="price" type="number" cssClass="span-input" id="optionPrice"/>
            <br>
            <br>
            <frm:errors path="price" cssClass="error"/>
        </label>
        <label class="label-input">
            <span class="span-input">Connection cost</span>
            <frm:input path="connectionCost" type="number" cssClass="span-input" id="optionConnectionPrice"/>
            <br>
            <br>
            <frm:errors path="connectionCost" cssClass="error" style="margin-left: 0px;"/>
        </label>
        <br>
    <button type="submit" class="button-2" style="margin-left: 50%;">Create</button>
</frm:form>



<%--<frm:form method="POST" action="${pageContext.request.contextPath}/tariff" modelAttribute="tariff" cssClass="form-style-4">--%>

<%--    <label for="field1" class="label-input">--%>
<%--        <span class="span-input">Name</span>--%>
<%--        <frm:input cssClass="span-input" path="name" type="text" id="tariffName" aria-describedby="TariffHelp" placeholder="max 30 characters"/>--%>
<%--        <br>--%>
<%--        <br>--%>
<%--        <frm:errors path="name" cssClass="error"/>--%>
<%--    </label>--%>
<%--    <label for="field2" class="label-input">--%>
<%--        <span class="span-input">Price</span>--%>
<%--        <frm:input cssClass="span-input" path="price" type="number" step="any" id="tariffPrice" placeholder="max 1000"/>--%>
<%--        <br>--%>
<%--        <br>--%>
<%--        <frm:errors path="price" cssClass="error"/>--%>
<%--    </label>--%>
<%--    <button type="submit" class="button-2" style="margin-left: 50%;">Create</button>--%>

<%--</frm:form>--%>




<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark" style="margin-top: -10px;">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">� 2020 T-Systems</p>
        </div>
    </div>
</footer>
</body>
</html>