<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>
<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <script src="<c:url value='/scripts/contractCreation.js' context='/eecare_war'/>"></script>
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/toggle-disabled.css' context='/eecare_war'/>">
    <script src="<c:url value='/scripts/contractOptions.js' context='/eecare_war'/>"></script>
    <script src="<c:url value='/scripts/creationWizard.js' context='/eecare_war'/>"></script>


    <title>Title</title>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/adminPage'" style="color: wheat; margin-left: 10%;">Back to admins page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat;">
        Select options
    </span>

    <p style="padding-top: 20px; margin-left: 200px;">
        <button class="button-2" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Shopping cart
        </button>
    </p>
    <div class="collapse" id="collapseExample" style="padding-left: 20px;">
        <div class="card card-body" style="background-color: wheat;height: max-content;width: max-content;padding-left: 10px;font-family: 'Chilanka', cursive;">
             - Tariff: ${selectedTariff.getName()} | Price: ${selectedTariff.getPrice()}
            <div id="optionsBlock">
                <c:if test="${selectedOptions == null || selectedOptions.isEmpty()}">
                    - No options were selected
                </c:if>
                <c:if test="${selectedOptions != null && !selectedOptions.isEmpty()}" >
                    - Options:
                    <ul>
                        <c:forEach items="${selectedOptions}" var="option">
                            <li>
                                    ${option.getName()}
                            </li>
                        </c:forEach>
                    </ul>
                </c:if>
            </div>
        </div>
    </div>

</nav>

<div class="container">
    <div class="row">
        <div class="col-sm" style="margin-top: 10px;">
            <button class="button-1" onclick="window.history.back()">Back</button>
        </div>
        <div class="col-sm-9">
            <div class="progress" style="margin-top: 3%; margin-bottom: 65px;">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%"></div>
            </div>
        </div>
        <div class="col-sm-1" style="margin-top: 10px;">
            <button class="button-1" onclick="getThirdPage(document)">Next</button>
        </div>
    </div>

</div>

<c:forEach items="${tariff.getOptionSet()}" var="option">
    <c:if test="${!option.getDeleted()}" >
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <div class="form-check form-check-inline" style="margin-left: 90%;">
                        <c:if test="${selectedOptions != null && !selectedOptions.isEmpty()}">
                            <c:choose>
                                <c:when test="${selectedOptions.contains(option)}">
<%--                                    <div class="custom-control custom-switch">--%>
<%--                                        <input type="checkbox" name="tariffOption" class="custom-control-input" id="customSwitch1" value="${option.getId()}" data-name="${option.getName()}" onchange="getOptionsAndSetCard(this)" checked>--%>
<%--                                    </div>--%>
                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" name="tariffOption" class="form-check-input" value="${option.getId()}" data-name="${option.getName()}" onchange="getOptionsAndSetCard(this)" checked>
                                </c:when>
                                <c:when test="${!selectedOptions.contains(option)}">
<%--                                    <div class="custom-control custom-switch">--%>
<%--                                        <input type="checkbox" name="tariffOption" class="custom-control-input" value="${option.getId()}" data-name="${option.getName()}" onchange="getOptionsAndSetCard(this)" data-value="test">--%>
<%--                                    </div>--%>
                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" name="tariffOption" class="form-check-input" value="${option.getId()}" data-name="${option.getName()}" onchange="getOptionsAndSetCard(this)">
                                </c:when>
                            </c:choose>
<%--                            <input type="checkbox" name="tariffOption" class="form-check-input" value="${option.getId()}" data-name="${option.getName()}" onchange="getOptionsAndSetCard(this)" checked>--%>
                        </c:if>
                        <c:if test="${selectedOptions == null || selectedOptions.isEmpty()}">
<%--                            <div class="custom-control custom-switch">--%>
<%--                                <input type="checkbox" name="tariffOption" class="custom-control-input" value="${option.getId()}" data-name="${option.getName()}" onchange="getOptionsAndSetCard(this)">--%>
<%--                            </div>--%>
                            <input type="checkbox" name="tariffOption" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" class="form-check-input" value="${option.getId()}" data-name="${option.getName()}" onchange="getOptionsAndSetCard(this)">
                        </c:if>
                        <label class="form-check-label"></label>
                    </div>
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" option-id="${option.getId()}" >
                    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                            ${option.getName()}
                    </span>
                        </button>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                    <span class="navbar-text; h3" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                        Price: ${option.getPrice()}
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </c:if>
</c:forEach>

<c:set var="selectedTariff" value="${tariff}" scope="session"></c:set>
<c:set var="newSelectedTariff" value="${tariff}" scope="session"></c:set>

<c:set var="tariff" value="${tariff}" scope="session"></c:set>


</body>
</html>
