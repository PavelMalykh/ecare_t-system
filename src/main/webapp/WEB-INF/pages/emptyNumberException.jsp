<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <div>
        <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

        <div id="empty-number-exception">
            <h1>
                Sorry, you need to input or generate a number.
            </h1>
        </div>
        <img src="<c:url value = '/img/wrong_number.png' context='/eecare_war'/>" id="icon" alt="User Icon1" />

<%--        <div class="modal" tabindex="-1" role="dialog" id="empty-number-modal">--%>
<%--            <div class="modal-dialog" role="document">--%>
<%--                <div class="modal-content">--%>
<%--                    <div class="modal-header" style="background-color: wheat;">--%>
<%--                        <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Sorry, you need to input or generate a number.</h5>--%>
<%--                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--%>
<%--                            <span aria-hidden="true">&times;</span>--%>
<%--                        </button>--%>
<%--                    </div>--%>
<%--                    <div class="modal-footer" style="background-color: black;">--%>
<%--                        <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;">Close</button>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
    </div>
</head>
<body>

</body>
</html>
