<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm"%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>

    <script src="<c:url value='/scripts/optionRules.js' context='/eecare_war'/>"></script>

</head>
<body>

<frm:form method="POST" action="${pageContext.request.contextPath}/option/rule">
    <pre>
            <form action="list">
                <h2> Main option </h2>
                <select name="main-option" id="main-option">
                    <c:forEach items="${options}" var="option">
                    <c:if test="${!option.getDeleted()}">
                        <option value="${option.getId()}">${option.getName()}</option>
                    </c:if>
                    </c:forEach>
                </select>
            </form>

            <form action="list">
                <h2> Obligatory options </h2>
                <select name="obligatory-option" multiple="multiple" id="obligatory-options-list">
                    <c:forEach items="${options}" var="option">
                        <c:if test="${!option.getDeleted()}">
                            <option value="${option.getId()}">${option.getName()}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </form>

            <form action="list">
                <h2> Incompatible options </h2>
                <select name="incompatible-option" multiple="multiple" id="incompatible-options-list">
                    <c:forEach items="${options}" var="option">
                        <c:if test="${!option.getDeleted()}">
                            <option value="${option.getId()}">${option.getName()}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </form>

            <button type="button" id="submit" class="btn btn-primary">Create rule</button>
            <br>
            <button type="button" id="return" class="btn btn-primary" onclick="window.location.href='/eecare_war/adminPage'">Back to admins page</button>

    </pre>
</frm:form>
</body>
</html>