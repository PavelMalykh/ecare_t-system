<%@ page import="org.springframework.security.authentication.UsernamePasswordAuthenticationToken" %>
<%@ page import="org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder" %>
<!doctype html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>


<html>
<head>
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">
    <script src="<c:url value='/scripts/blockUser.js' context='/eecare_war'/>"></script>
    <script src="<c:url value='/scripts/updateCustomer.js' context='/eecare_war'/>"></script>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>

<%
    Object userPrincipal = request.getUserPrincipal();
    Boolean isAdmin = ((UsernamePasswordAuthenticationToken) userPrincipal).getAuthorities().toString().equals("[ADMIN]");
    request.setAttribute("isAdmin", isAdmin);
%>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.history.back()" style="color: wheat; margin-left: 10%;">Back</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 27%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Profile
    </span>

    <c:if test="${customer.getUser().getEnabled() && isAdmin}">
        <button type="button" id="block-user" class="button-2" onclick="blockUser()">Block user</button>
    </c:if>
    <c:if test="${!customer.getUser().getEnabled() && isAdmin}">
        <button type="button" id="block-user" class="button-2" onclick="blockUser()">Unblock user</button>
    </c:if>
</nav>

<form class="form-style-4" style="margin-top: -150px;height: 940px;">

<%--    <label class="label-input">--%>
<%--        <span class="span-input">Id</span>--%>
        <input type="text" class="span-input" id="customer-id" aria-describedby="emailHelp" value="${customer.getId()}" readonly hidden>

    <input type="text" class="span-input" id="customer-password" aria-describedby="emailHelp" value="${customer.getUser().getPassword()}" readonly hidden>
    <input type="text" class="span-input" id="customer-birthday" aria-describedby="emailHelp" value="${customer.getBirthday()}" readonly hidden>


<%--    </label>--%>
    <label class="label-input">
        <span class="span-input">Firstname</span>
        <input type="text" class="span-input" id="customer-name" aria-describedby="emailHelp" value="${customer.getFirstName()}">
    </label>
    <label class="label-input">
        <span class="span-input">Lastname</span>
        <input type="text" class="span-input" id="customer-lastname" aria-describedby="emailHelp" value="${customer.getLastName()}">
    </label>
    <label class="label-input">
        <span class="span-input">Email</span>
        <input type="text" class="span-input" id="customer-email" aria-describedby="emailHelp" value="${customer.getUser().getEmail()}">
    </label>
    <label class="label-input">
        <span class="span-input">Address</span>
        <input type="text" class="span-input" id="customer-address" aria-describedby="emailHelp" value="${customer.getAddress()}">
    </label>
    <label class="label-input">
        <span class="span-input">Passport</span>
        <input type="text" class="span-input" id="customer-passport" aria-describedby="emailHelp" value="${customer.getPassportData()}">
    </label>
    <label class="label-input">
        <span class="span-input">Active</span>
        <input type="text" class="span-input" id="customer-isActive" aria-describedby="emailHelp" is-blocked="${!customer.getUser().getEnabled()}" value="${customer.getUser().getEnabled()}" readonly>
    </label>
    <div class="container">
        <div class="row">
            <div class="col-sm-7" style="padding-left: 450px;">
                <button type="button" id="update" class="button-2" onclick="updateCustomer()">Update</button>
            </div>
        </div>
    </div>
</form>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

<div class="modal" tabindex="-1" role="dialog" id="success">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <c:if test="${!customer.getUser().getEnabled()}">
                    <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Customer was successfully unblocked</h5>
                </c:if>
                <c:if test="${customer.getUser().getEnabled()}">
                    <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Customer was successfully blocked</h5>
                </c:if>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/customer/${customer.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/customer/${customer.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="success-update">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Customer was successfully updated</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/customer/${customer.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/customer/${customer.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="fail-update-phone">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Sorry, email has already been taken, please, try another one</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/customer/${customer.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/customer/${customer.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
