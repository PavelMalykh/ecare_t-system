<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="<c:url value='/scripts/myContract.js' context='/eecare_war'/>"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>
<link rel="stylesheet" href="<c:url value='/styles/main.css' context='/eecare_war'/>">
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/adminPage'" style="color: wheat; margin-left: 10%;">Back to admin page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Contracts
    </span>

</nav>

<table class="table" tyle="font-family: 'Chilanka', cursive;">
    <thead class="thead-dark h4">
    <tr>
        <th scope="col">Phone</th>
        <th scope="col">Customer</th>
        <th scope="col">Tariff name</th>
        <th scope="col">Tariff price</th>
        <th scope="col">Options</th>
        <th scope="col">Deleted</th>
        <th scope="col" style="text-align: right;">Action</th>
    </tr>
    </thead>
    <tbody style="font-size: 25px;">
        <c:forEach items="${contracts}" var="contract">
            <tr>
                <td>${contract.getPhone()}</td>
                <th>${contract.getCustomer().getFirstName()}</th>
                <td>${contract.getTariff().getName()}</td>
                <td>${contract.getTariff().getPrice()}</td>
                <td>
                    <c:forEach items="${contract.getOptions()}" var = "contractItem">
                        <ul>
                            <li>${contractItem.getName()}</li>
                        </ul>
                    </c:forEach>
                </td>
                <th>${contract.getDeleted()}</th>
                <th style="text-align: -webkit-right; margin: 50px">
                    <div class="container1">
                        <button type="button" id="edit" class="button-1" onclick="window.location='/eecare_war/contract/${contract.getId()}'">Edit</button>
                    </div>
                </th>
            </tr>
        </c:forEach>
    </tbody>
</table>

<footer class="footer" style="position: fixed;">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

</body>
</html>
