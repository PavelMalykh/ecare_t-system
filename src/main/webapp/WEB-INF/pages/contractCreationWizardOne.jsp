<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <script src="<c:url value='/scripts/creationWizard.js' context='/eecare_war'/>"></script>
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/adminPage'" style="color: wheat; margin-left: 10%;">Back to admins page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 15%; margin-top: 15px;color: wheat;">
        Select tariff
    </span>

    <p style="padding-top: 20px; margin-left: 200px;">
        <button class="button-2" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Shopping cart
        </button>
    </p>
    <div class="collapse" id="collapseExample" style="padding-left: 20px;">
        <div class="card card-body" style="background-color: wheat;height: max-content;width: max-content;padding-left: 10px;font-family: 'Chilanka', cursive;">
            <c:if test="${selectedTariff == null}">
                You need to select a tariff and options
            </c:if>
            <c:if test="${selectedTariff != null}">
                - Tariff: ${selectedTariff.getName()} | Price: ${selectedTariff.getPrice()}
            </c:if>
            <br>
            <c:if test="${selectedOptions == null || selectedOptions.isEmpty()}">
                - No options were selected
            </c:if>
            <c:if test="${selectedOptions != null && !selectedOptions.isEmpty()}" >
                - Options:
                <ul>
                    <c:forEach items="${selectedOptions}" var="option">
                        <li>
                                ${option.getName()}
                        </li>
                    </c:forEach>
                </ul>
            </c:if>
        </div>
    </div>

<%--    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/adminPage'" style="color: wheat; margin-left: 10%;">Back to admins page</button>--%>


</nav>

<div class="container">
    <div class="row">
        <div class="col-sm-2" style="margin-top: 10px;">
            <button class="button-1" onclick="window.history.back()">Back</button>
        </div>
        <div class="col-sm">
            <div class="progress" style="margin-top: 3%; margin-bottom: 65px;">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 25%"></div>
            </div>
        </div>
    </div>

</div>

<%--        <div class="progress" style="width: 50%;margin-left: 30%;margin-top: 3%; margin-bottom: 65px;">--%>
<%--            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 25%"></div>--%>
<%--        </div>--%>

<c:forEach items="${tariffs}" var="tariff">
    <c:if test="${!tariff.getDeleted()}">
    <div class="accordion" id="accordionExample">
    <div class="card">
        <div class="card-header" id="headingOne">
            <button type="button" class="button-3" style="margin-left: 90%;background-color: #353a40" tariff-id="${tariff.getId()}" onclick="secondPage(this)">Select</button>
            <h5 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                    ${tariff.getName()}
                </span>
                </button>
            </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <span class="navbar-text; h3" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                    Price: ${tariff.getPrice()}
                </span>
            </div>
        </div>
    </div>

</div>
    </c:if>

    <c:set var="customer" value="${customer}" scope="session"></c:set>


</c:forEach>

</body>
</html>
