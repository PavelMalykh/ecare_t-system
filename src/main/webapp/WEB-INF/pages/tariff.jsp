<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">

<script src="<c:url value='/scripts/deleteTariff.js' context='/eecare_war'/>"></script>
<script src="<c:url value='/scripts/updateTar.js' context='/eecare_war'/>"></script>

<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/tariff'" style="color: wheat; margin-left: 10%;">Back to tariffs page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        ${tariff.getName()}
    </span>
    <c:if test="${!tariff.getDeleted()}" >
    <button type="button" id="delete" class="button-2" onclick="deleteTariff()" style="color: wheat">Delete tariff</button>
    </c:if>

</nav>



<form class="form-style-4" style="padding-top: 100px;height: 1080px;">

<%--    <label class="label-input">--%>
<%--        <span class="span-input">Id</span>--%>
        <input type="text" class="form-control" id="tariff-id" aria-describedby="emailHelp" value="${tariff.getId()}"readonly hidden>
<%--        <br>--%>
<%--        <br>--%>
<%--    </label>--%>
    <label class="label-input">
        <span class="span-input">Name</span>
        <input type="text" class="form-control" id="tariff-name" aria-describedby="emailHelp" value="${tariff.getName()}" >
        <br>
        <br>
    </label>
    <label class="label-input">
        <span class="span-input">Price</span>
        <input type="text" class="form-control" id="tariff-price" aria-describedby="emailHelp" value="${tariff.getPrice()}">
        <br>
        <br>
    </label>
    <label class="label-input" style="margin-bottom: -50px;">
        <span class="span-input">Deleted</span>
        <input type="text" class="form-control" id="tariff-deleted" aria-describedby="emailHelp" value="${tariff.getDeleted()}"readonly>
        <br>
        <br>
    </label>
    <div style="padding-left: 575px;font-size: 40px;font-family: cursive;padding-bottom: 10px;"> Options </div>
    <div style="padding-left: 578px;padding-top: 20px;font-family: 'Chilanka', cursive;font-size: 25px;padding-bottom: 40px;color: wheat;">
        <c:if test="${!tariff.getOptionSet().isEmpty()}">
            <c:forEach items="${tariff.getOptionSet()}" var = "option">
                <ul>
                    <li>${option.getName()}</li>
                </ul>
            </c:forEach>
        </c:if>
        <c:if test="${tariff.getOptionSet().isEmpty()}">
            No options were included
        </c:if>
    </div>
<%--    <label class="label-input">--%>
<%--        <span class="span-input">Options</span>--%>
<%--        <select class="selectpicker" multiple data-live-search="true" id="options" style="font-family: 'Chilanka', cursive;">--%>
<%--            <c:forEach items="${tariff.getOptionSet()}" var="option">--%>
<%--                <option style="font-family: 'Chilanka', cursive;" value="${option.getId()}" selected disabled name="tariffOption">${option.getName()}</option>--%>
<%--            </c:forEach>--%>
<%--        </select>--%>
<%--        <br>--%>
<%--        <br>--%>
<%--    </label>--%>
    <div class="container">
        <div class="row">
            <div class="col-sm-6" style="padding-left: 400px;">
                <button type="button" id="update" class="button-2" onclick="updateTariff()">Update</button>
            </div>
            <div class="col-sm" style="padding-left: 80px;">
                <button type="button" id="change-options" class="button-2" onclick="window.location='/eecare_war/option/change'">Change options</button>
            </div>
        </div>
    </div>
</form>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

<div class="modal" tabindex="-1" role="dialog" id="success">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Tariff was successfully deleted</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/tariff/${tariff.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/tariff/${tariff.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="success-updated">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Tariff was successfully updated</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/tariff/${tariff.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/tariff/${tariff.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="failed-updated-price">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Tariff price should be less or equals than 1000</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/tariff/${tariff.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/tariff/${tariff.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="failed-updated-name">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Sorry, name should be unique</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/tariff/${tariff.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/tariff/${tariff.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>


<c:set var="tariff" value="${tariff}" scope="session"></c:set>
<c:set var="optionSet" value="${tariff.getOptionSet()}" scope="session"></c:set>

<c:forEach items="${tariff.getOptionSet()}" var="option">
    <c:forEach items="${option.getIncompatibleOption()}" var="incompatibleOption">
        <c:set var="incompatibleOptionsTariff" value="${incompatibleOption.getName()}" scope="session"></c:set>
    </c:forEach>
</c:forEach>

<c:forEach items="${tariff.getOptionSet()}" var="option">
    <c:forEach items="${option.getObligatoryOption()}" var="obligatoryOption">
        <c:set var="obligatoryOptionsTariff" value="${obligatoryOption.getName()}" scope="session"></c:set>
    </c:forEach>
</c:forEach>

</body>
</html>
