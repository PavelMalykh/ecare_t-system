<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">

<script src="<c:url value='/scripts/selectTariff.js' context='/eecare_war'/>"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/main.css' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/adminPage'" style="color: wheat; margin-left: 10%;">Back to admin page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Tariffs
    </span>

    <button type="button" id="create" class="button-2" onclick="window.location='${pageContext.request.contextPath}/tariff/creation'" style="color: wheat">Create new tariff</button>

</nav>

    <table class="table" style="font-family: 'Chilanka', cursive;">
        <thead class="thead-dark h4">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Options</th>
            <th scope="col" style="text-align: right;">Action</th>
        </tr>
        </thead>
        <tbody style="font-size: 25px;">
        <c:forEach items="${tariffs}" var="tariff">
            <tr>
                <td>${tariff.getName()}</td>
                <th>${tariff.getPrice()}</th>
                <td>
                    <c:forEach items="${tariff.getOptionSet()}" var = "option">
                        <ul>
                            <li>${option.getName()}</li>
                        </ul>
                    </c:forEach>
                </td>
                <th style="text-align: -webkit-right;">
                    <div class="container1">
                        <button type="button" id="edit" class="button-1" onclick="window.location='/eecare_war/tariff/${tariff.getId()}'">Edit</button>
                    </div>
                </th>
            </tr>
        </c:forEach>
        </tbody>
    </table>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

</body>
</html>
