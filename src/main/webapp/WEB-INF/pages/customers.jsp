<!doctype html>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="<c:url value='/scripts/selectCustomer.js' context='/eecare_war'/>"></script>
<script src="<c:url value='/scripts/creationWizard.js' context='/eecare_war'/>"></script>

<script src="https://code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/main.css' context='/eecare_war'/>">
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/adminPage'" style="color: wheat; margin-left: 10%;">Back to admins page</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 20%; margin-top: 15px;color: wheat; margin-right: 20%;">
        Customers
    </span>
</nav>

<table class="table" style="font-family: 'Chilanka', cursive;">
    <thead class="thead-dark h4">
    <tr>
        <th scope="col">Firstname</th>
        <th scope="col">Lastname</th>
        <th scope="col">Email</th>
        <th scope="col">Birthday</th>
        <th scope="col">Passport</th>
        <th scope="col">Address</th>
        <th scope="col" style="text-align: right;">Action</th>
    </tr>
    </thead>
    <tbody style="font-size: 25px;">
    <c:forEach items="${customers}" var="customer">
        <tr>
            <td>${customer.getFirstName()}</td>
            <th>${customer.getLastName()}</th>
            <th>${customer.getUser().getEmail()}</th>
            <th>${customer.getBirthday()}</th>
            <th>${customer.getPassportData()}</th>
            <th>${customer.getAddress()}</th>
<%--            <th>--%>
            <th style="text-align: -webkit-right; margin: 50px">
                <div class="container1">
                    <button type="button" id="edit" class="button-1" onclick="window.location='/eecare_war/customer/${customer.getId()}'">Edit</button>
                </div>
                <br>
                <div class="container1">
                    <button type="button" id="create" class="button-1" onclick="firstPage(this)" customer-id="${customer.getId()}" style="width: 116px;display: flex;justify-content: center;">Add contract</button>
                </div>
            </th>
<%--                <div class="container1">--%>
<%--                    <img class="image" src="<c:url value = '/img/edit.png' context='/eecare_war'/>" id="icon" alt="User Icon1" width="2%" aria-placeholder="edit"/>--%>
<%--                    <div class="overlay">--%>
<%--                        <div>--%>
<%--                            <a class="text" href="/eecare_war/customer/${customer.getId()}">Edit</a>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="container1">--%>
<%--                    <img class="image" src="<c:url value = '/img/create.jpeg' context='/eecare_war'/>" id="icon-create" alt="User Icon1" style="width: 12%;" aria-placeholder="edit"/>--%>
<%--                    <div class="overlay">--%>
<%--                        <div>--%>
<%--                            <a class="text" style="font-size: 10px;" onclick="firstPage(this)" customer-id="${customer.getId()}">Create contract</a>--%>
<%--                        </div>--%>
<%--                </div>--%>
<%--                </div>--%>
<%--            </th>--%>
        </tr>
    </c:forEach>
    </tbody>
</table>
<%--<button type="button" id="return" class="btn btn-primary" onclick="window.location='/eecare_war/adminPage'">Back to admin page</button>--%>

<footer class="footer" style="position: fixed;">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

</body>
</html>
