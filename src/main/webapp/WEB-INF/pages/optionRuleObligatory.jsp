<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm"%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<%--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--%>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>--%>
<%--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>--%>

<%--<script src="https://code.jquery.com/jquery-1.10.2.js"--%>
<%--        type="text/javascript"></script>--%>
<%--<html>--%>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/input.css' context='/eecare_war'/>">

    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


<%--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">--%>
<%--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />--%>
<%--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--%>
<%--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>--%>
<%--    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>--%>

    <title>Title</title>

    <script src="<c:url value='/scripts/updateOptionsForOption.js' context='/eecare_war'/>"></script>

</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

    <button type="button" id="return" class="button-2" onclick="window.location='/eecare_war/option/${myOption.getId()}'" style="color: wheat; margin-left: 10%;">Back to option</button>

    <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-left: 15%; margin-top: 15px;color: wheat; margin-right: 20%;font-size: 32px;">
        Obligatory options for (${option.getName()})
    </span>

    <button type="button" id="submit" class="button-2" style="margin-left: -80px;" onclick="updateOptionsObligatory(document)">Update</button>

</nav>

            <input type="text" class="form-control" id="main-option" aria-describedby="emailHelp" value="${option.getId()}" hidden>
            <input type="text" class="form-control" id="main-option-name" aria-describedby="emailHelp" value="${option.getName()}"hidden>

    <c:forEach items="${options}" var="option">
        <c:if test="${!option.getDeleted() && !myOption.equals(option)}" >
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <div class="form-check form-check-inline" style="margin-left: 90%;">
                            <c:choose>
                                <c:when test="${myIncompatibleOptions.contains(option)}">
                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" name="tariffOption" class="form-check-input" value="${option.getId()}" disabled>
                                </c:when>
                                <c:when test="${myObligatoryOptions.contains(option)}">
                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" name="tariffOption" class="form-check-input" value="${option.getId()}" checked>
                                </c:when>
                                <c:when test="${!myObligatoryOptions.contains(option)}">
                                    <input type="checkbox" data-toggle="toggle" data-onstyle="outline-warning" data-offstyle="outline-info" name="tariffOption" class="form-check-input" value="${option.getId()}" >
                                </c:when>
<%--                         <c:if test="${myObligatoryOptions.contains(option)}">--%>
<%--                            <input type="checkbox" name="tariffOption" class="form-check-input" value="${option.getId()}" checked>--%>
<%--                         </c:if>--%>
<%--                        <c:if test="${!myObligatoryOptions.contains(option)}">--%>
<%--                            <input type="checkbox" name="tariffOption" class="form-check-input" value="${option.getId()}" >--%>
<%--                        </c:if>--%>
<%--                        <c:if test="${myIncompatibleOptions.contains(option)}">--%>
<%--                            <input type="checkbox" name="tariffOption" class="form-check-input" value="${option.getId()}" disabled>--%>
<%--                        </c:if>--%>
                            </c:choose>
                            <label class="form-check-label"></label>
                        </div>
                            <%--                    <button type="button" class="btn btn-secondary" style="margin-left: 90%;">Select</button>--%>
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" option-id="${option.getId()}" >
                        <span class="navbar-text; h1" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                                ${option.getName()}
                        </span>
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                        <span class="navbar-text; h3" style="font-family: 'Chilanka', cursive; margin-top: 15px;color: black;">
                            Price: ${option.getPrice()}
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
    </c:forEach>

<%--        <div class="form-group">--%>
<%--        <h2> Obligatory options </h2>--%>
<%--            <select class="selectpicker" data-live-search="true" multiple id="obligatory-options-list" name="obligatory-option">--%>
<%--                <c:forEach items="${options}" var="option">--%>
<%--                    <c:if test="${!option.getDeleted()}">--%>
<%--                        <c:choose>--%>
<%--                            <c:when test="${myObligatoryOptions.contains(option)}">--%>
<%--                                <option value="${option.getId()}" selected>${option.getName()}</option>--%>
<%--                            </c:when>--%>
<%--                            <c:otherwise>--%>
<%--                                <option value="${option.getId()}">${option.getName()}</option>--%>
<%--                            </c:otherwise>--%>
<%--                        </c:choose>--%>
<%--                    </c:if>--%>
<%--                </c:forEach>--%>
<%--            </select>--%>
<%--        </div>--%>

<%--        <div class="form-group">--%>
<%--        <h2> Incompatible options </h2>--%>
<%--            <select class="selectpicker" data-live-search="true" multiple id="incompatible-options-list" name="incompatible-option">--%>
<%--                    <c:forEach items="${options}" var="option">--%>
<%--                        <c:if test="${!option.getDeleted()}">--%>
<%--                            <c:choose>--%>
<%--                                <c:when test="${myIncompatibleOptions.contains(option)}">--%>
<%--                                    <option value="${option.getId()}" selected>${option.getName()}</option>--%>
<%--                                </c:when>--%>
<%--                                <c:otherwise>--%>
<%--                                    <option value="${option.getId()}">${option.getName()}</option>--%>
<%--                                </c:otherwise>--%>
<%--                            </c:choose>--%>
<%--                        </c:if>--%>
<%--                    </c:forEach>--%>
<%--            </select>--%>

<%--        </div>--%>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>

<div class="modal" tabindex="-1" role="dialog" id="success-obligatory">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: wheat;">
                <h5 class="modal-title" style="margin-left: 20%; font-family: 'Chilanka', cursive;">Options were successfully updated</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.location='/eecare_war/option/${option.getId()}'">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer" style="background-color: black;">
                <button type="button" class="button-2" data-dismiss="modal" style="margin-right: 165px;" onclick="window.location='/eecare_war/option/${option.getId()}'">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>