<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Chilanka&display=swap" rel="stylesheet">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/buttom.css' context='/eecare_war'/>">
    <link rel="stylesheet" href="<c:url value='/styles/background.css' context='/eecare_war'/>">


    <title>Title</title>
    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>
    <script src="<c:url value='/scripts/searchByPhone.js' context='/eecare_war'/>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

</head>
<body style="background-color: #f7f7f7;">


<nav class="navbar navbar-expand-sm bg-dark navbar-dark h3" id="admin-panel">
    <a class="navbar-brand" style="font-family: 'Chilanka', cursive; color: wheat;font-size: 28px;" >User page</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="font-family: 'Chilanka', cursive;padding: 10px 10px;">
        <ul class="navbar-nav mr-auto" >
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/contract/mine">My contracts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/customer/profile">Profile</a>
            </li>
            <li class="nav-item" style="padding-left: 1060px;">
                <button class="button-2" type="button" id="logout" onclick="window.location.href='${pageContext.request.contextPath}/main'">Logout</button>
            </li>
        </ul>
    </div>
</nav>

    <div class="background" style="height: 800px;margin-left: 350px;">

    </div>

<footer class="footer">
    <div class="container-fluid text-center py-3 navbar-dark bg-dark">
        <div class="footer-text">
            <p class="card-text" style="font-family: 'Chilanka', cursive;color: wheat;font-size: 20px;">© 2020 T-Systems</p>
        </div>
    </div>
</footer>
</body>
</html>
