<%--
  Created by IntelliJ IDEA.
  User: pavel.malykh
  Date: 06/10/2020
  Time: 16:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <link rel="icon" type="image/svg"  href="<c:url value='/img/sup.svg' context='/eecare_war'/>">

    <title>Title</title>
</head>
<body>

<%
%>

<h1>
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Success!</h4>
        <p>Contract was successfully created</p>
        <hr>
        <p class="mb-0">
            <a href="${pageContext.request.contextPath}/contract">Return to contracts page</a>
        </p>
    </div>
</h1>


</body>
</html>
