function updateOptionsForContract() {


        var contractId = $("#contract-id").val();

        var options = $("[name='tariffOption']", document)

        var optionList = []

        for (var i = 0; i < options.length; i++) {
            if (options.get(i).checked) {
                optionList.push(options.get(i).value)
            }
        }


        $.ajax({
            url: "/eecare_war/contract/changeOptions",
            method: "POST",
            data: {"contractId": contractId,
                "options" : optionList.toString()
            },
            success: function () {
                $("#success").modal('show')
            },
            error: function () {
                alert("ERROR")
            }
        })

}