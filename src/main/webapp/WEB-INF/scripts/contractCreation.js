$(document).ready(function () {

    var linkingButton = $("#submit");

    linkingButton.click(function () {

        var customer = $("#customer").val();
        var options = $('.selected-options');
        var tariff = $("#tariff").val();
        var phone = $("#phone").val();


        var optionList = []

        for (var i = 0; i < options.length; i++) {
            optionList.push(options.get(i).value)
        }

        console.log(optionList)

        // var incompatibleList = $("#incompatible-list").val();
        // var obligatoryList = $("#obligatory-list").val();
        //
        // // if (obligatoryList == null) {
        // //     obligatoryList = "";
        // // }
        //
        // if (incompatibleList == null) {
        //     obligatoryList = "";
        // }
        //
        // if (optionList == null) {
        //     optionList = "";
        // }
        //
        // console.log(obligatoryList)
        // console.log(optionList)

        $.ajax({
            url: "/eecare_war/contract",
            method: "POST",
            data: {"tariff": tariff,
                "options" : optionList.toString(),
                "customer" : customer,
                "phone" : phone
            },
            success: function (text) {

                var emptyNumber = $(text).find('#empty-number-exception');
                var wrongNumber = $(text).find('#wrong-number-exception');
                var emtytariff = $(text).find('#empty-tariff-exception')
                var alreadyTakenPhoneError = $(text).find('#phone-taken-email');
                var deletedTariffException = $(text).find('#deleted-tariff');
                var deletedOptionException = $(text).find('#deleted-option');


                if (emptyNumber.length > 0) {
                    $("#phone").css('background-color', 'lightcoral')
                    $("#empty-number-modal").modal('show')
                } else if (wrongNumber.length > 0) {
                    $("#phone").css('background-color', 'lightcoral')
                    $("#incorrect-format-modal").modal('show')
                } else if (emtytariff.length > 0) {
                    alert("Sorry, you need to select a tariff")
                } else if(alreadyTakenPhoneError.length > 0) {
                    $("#phone").css('background-color', 'lightcoral')
                    $("#already-taken-modal").modal('show')
                } else if(deletedTariffException.length > 0) {
                    $("#deleted-tariff").modal('show')
                } else if(deletedOptionException.length > 0) {
                    $("#deleted-option").modal('show')
                }
                else {
                    console.log("SUCCESS")
                    console.log(text)
                    $("#success-creation").modal('show')
                }

            },
            error: function () {
                alert("ERROR")
            }
        })
    })

    var generateButton = $("#generate");

    generateButton.click(function () {

        var phoneNumber = $("#phone").val();
        var phoneNumberInput = $("#phone");

        $.ajax({
            url: "/eecare_war/phone/generate",
            method: "POST",
            data: {
                "phone" : phoneNumber
            },
            success: function (text) {
                var phone = $(text).find("#phone").val();
                $("#phone").css('background-color', 'greenyellow')
                phoneNumberInput.val(phone)
            },
            error: function (text) {
                alert("FAILED")
            }

        })
    })

    var validatePhoneButton = $("#check-number");

    validatePhoneButton.click(function () {

        var phoneNumber = $("#phone").val();

        $.ajax({
            url: "/eecare_war/phone",
            method: "GET",
            data: {
                "phone" : phoneNumber
            },
            success: function (text) {
                var alreadyTakenPhoneError = $(text).find('#phone-taken-email');
                var emptyNumberException = $(text).find('#empty-number-exception')

                if (alreadyTakenPhoneError.length > 0) {
                    $("#phone").css('background-color', 'lightcoral')
                    $("#already-taken-modal").modal('show')
                }  else if(emptyNumberException.length > 0) {
                    $("#phone").css('background-color', 'lightcoral')
                    $("#empty-number-modal").modal('show')
                }

                    else {

                    var isOk = $(text).find("#phone").attr("data-id");

                    if (isOk == "false") {
                        $("#phone").css('background-color', 'lightcoral')
                        $("#incorrect-format-modal").modal('show')
                        // alert("Incorrect number, please try another (example: 8-800-555-35-35)")
                    } else {
                        $("#phone").css('background-color', 'greenyellow')
                        $("#ok-modal").modal('show')
                    }
                }
            }
        })

    })
})

function getOptions(text) {


    var optionId = text.value
    console.log(optionId)

    $.ajax({
        url: "/eecare_war/option/getOptions/" + optionId,
        method: "GET",
        success:function () {
            alert("URA")
        }
    })


    // var tariffId = $("#tariff").val();
    //
    // var optionList = $("#options-list");
    //
    // var optionListBlock = $("#options-list-block");
    // var incompatibleListBlock = $("#incompatible-list-block")
    // var obligatoryListBlock = $("#obligatory-list-block")
    //
    // if (tariffId === "") {
    //     optionListBlock.hide()
    //     incompatibleListBlock.hide()
    //     obligatoryListBlock.hide()
    //
    // } else {
    //
    //     $.ajax({
    //             url: "/eecare_war/tariff/" + tariffId,
    //             method: "GET",
    //             success: function (text) {
    //                 var x = $(text).find("#options").html();
    //
    //                 // var options = console.log($(text).getElementsByName('tariffOption'))
    //
    //                 var z = $("[name='tariffOption']", text)
    //
    //                 for(var i = 0; i < z.length; i++) {
    //                     z.get(i).disabled = false
    //                     z.get(i).selected = false
    //                 }
    //                 console.log(z)
    //
    //                 if (x.trim() === "") {
    //                     optionListBlock.hide()
    //                     incompatibleListBlock.hide()
    //                     obligatoryListBlock.hide()
    //                 } else {
    //                     optionListBlock.show();
    //                     incompatibleListBlock.show();
    //                     obligatoryListBlock.show()
    //                     optionList.html(z);
    //                 }
    //             }
    //         }
    //     )
    // }
    // showTariffAndPhone()
}

function updateContractTariff(text) {

    var tariffId = text.value

        var contractId = $("#contract-id").val();
        var tariffId = $("#tariff-id").val();

        $.ajax({
            url: "/eecare_war/contract/" + contractId,
            method: "POST",
            data: {
                   // "options" : optionList.toString(),
                   "tariffId" : tariffId
            },
            success: function () {
                $("#success-updated").modal('show')
            },
            error: function () {
                alert("ERROR")
            }
        })
}

function showTariffAndPhone() {
    var customerID = $("#customer").val();

    var tariffBlock = $("#tariff-block")
    var phoneBlock = $("#phone-block")
    var submitBlock = $("#submit-block")

    if (customerID === "") {
        tariffBlock.hide()
        phoneBlock.hide()
        submitBlock.hide()
    } else {
        tariffBlock.show()
        phoneBlock.show()
        submitBlock.show()
    }

}