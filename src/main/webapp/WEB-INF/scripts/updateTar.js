function updateTariff() {

    var tariffId = $("#tariff-id").val();
    var tariffPrice = $("#tariff-price").val();
    var tariffName = $("#tariff-name").val();

    $.ajax({
        url: "/eecare_war/tariff/changed/" + tariffId,
        method: "POST",
        data: {
            "id" : tariffId,
            "name" : tariffName,
            "price" : tariffPrice
        },
        success: function (response) {

            var bigOptionPriceException = $(response).find('#big-tariff-price-exception');
            var dublicateUnique = $(response).find('#already-taken-email');

            console.log(response)


            if (bigOptionPriceException.length > 0) {
                $("#failed-updated-price").modal('show')
            } else if(dublicateUnique.length > 0) {
                console.log(dublicateUnique.length)
                $("#failed-updated-name").modal('show')
            }
            else {
                $("#success-updated").modal('show')
            }
        },
        error: function () {
            alert("ERROR")
        }
    })
}