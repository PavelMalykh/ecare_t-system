function updateOptionsTariff(text) {

    var options = $("[name='tariffOption']", text)

    var optionsId = []

    for (var i = 0; i < options.length; i++) {
        if (options.get(i).checked) {
            optionsId.push(options.get(i).value)
        }
    }

    var tariffId = $("#tariff-id").val();

    console.log(optionsId)
    console.log(tariffId)

    $.ajax({
        url: "/eecare_war/tariff/" + tariffId,
        method: "POST",
        data: {
            "options" : optionsId.toString(),
            "tariffId" : tariffId
        },
        success: function () {
            $("#success-updated").modal('show')
        },
        error: function () {
            alert("ERROR")
        }
    })
}