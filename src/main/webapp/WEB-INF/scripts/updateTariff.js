function updateTariff() {

    var tariffId = $("#tariff-id").val();
    var optionList = $("#options-list").val();

    if (optionList == null) {
        optionList = ""
    }

    $.ajax({
        url: "/eecare_war/tariff/" + tariffId,
        method: "POST",
        data: {
            "options" : optionList.toString(),
            "tariffId" : tariffId
        },
        success: function () {
            window.location.href = "/eecare_war/tariff/" + tariffId
        },
        error: function () {
            alert("ERROR")
        }
    })
}