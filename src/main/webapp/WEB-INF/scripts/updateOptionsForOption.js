function updateOptionsObligatory(text) {

    var mainOption = $(text).find('#main-option').val();

    var options = $("[name='tariffOption']", text)

    var optionsId = []

    var incompatibleId = []

    for (var i = 0; i < options.length; i++) {
        if (options.get(i).checked) {
            optionsId.push(options.get(i).value)
        }
    }

    $.ajax({
        url: "/eecare_war/option/rule",
        method: "POST",
        data: {
            "option": mainOption,
            "obligatoryOptions": optionsId.toString(),
            "incompatibleOptions": incompatibleId.toString()
        },
        success: function () {
            $("#success-obligatory").modal('show')
        },
        error: function () {
            alert("BAD")
        }
    })
}

function updateOptionsIncompatible(text) {

    var mainOption = $(text).find('#main-option').val();

    var options = $("[name='tariffOption']", text)

    var optionsId = []

    var obligatoryId = []

    for (var i = 0; i < options.length; i++) {
        if (options.get(i).checked) {
            optionsId.push(options.get(i).value)
        }
    }

    $.ajax({
        url: "/eecare_war/option/rule",
        method: "POST",
        data: {
            "option": mainOption,
            "obligatoryOptions": obligatoryId.toString(),
            "incompatibleOptions": optionsId.toString()
        },
        success: function () {
            $("#success-incompatible").modal('show')
        },
        error: function () {
            alert("BAD")
        }
    })
}