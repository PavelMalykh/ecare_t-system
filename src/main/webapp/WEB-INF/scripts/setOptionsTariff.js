function setOptions(text) {

    if (text.checked) {
        var optionId = text.value

        $.ajax({
            url: "/eecare_war/option/getOptions/" + optionId,
            method: "GET",
            success: function (text) {
                var obligatoryOptions = $("[name='ObligatoryOption']", text)
                var obligatoryOptionIds = []
                for (var i = 0; i < obligatoryOptions.length; i++) {
                    obligatoryOptionIds.push(obligatoryOptions[i].value);
                }

                for (var i = 0; i < obligatoryOptionIds.length; i++) {
                    if (i == 0) {
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").checked = true
                    } else {
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").parentElement.className = 'toggle btn btn-outline-warning'
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").parentElement.style.opacity="0.4"
                        console.log(document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']"))
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").checked = true
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").disabled = true
                    }
                }
            }
        })

    } else {
        var optionId = text.value

        $.ajax({
            url: "/eecare_war/option/getOptions/" + optionId,
            method: "GET",
            success: function (text) {
                var obligatoryOptions = $("[name='ObligatoryOption']", text)
                var obligatoryOptionIds = []
                for (var i = 0; i < obligatoryOptions.length; i++) {
                    obligatoryOptionIds.push(obligatoryOptions[i].value);
                }

                for (var i = 0; i < obligatoryOptionIds.length; i++) {
                    if (i == 0) {
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").checked = false
                    } else {
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").parentElement.style.opacity = "1"
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").parentElement.className = 'toggle btn btn-outline-info off'
                        console.log(document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']"))
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").checked = false
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").disabled = false
                    }
                }
            }
        })


    }
}