function blockUser() {
    var isActive = $("#customer-isActive").attr("is-blocked");
    var email = $("#customer-email").val();

    $.ajax({
        url: "/eecare_war/customer/block",
        method: "POST",
        data: {
            "email" : email,
            "isActive": isActive
        },
        success: function () {
            $("#success").modal('show')
        },
        error: function () {
            alert("FAILED")
        }
    })
}