function updateCustomer() {
    var name = $("#customer-name").val();
    var lastname = $("#customer-lastname").val();
    var address = $("#customer-address").val();
    var email = $("#customer-email").val();
    var passport = $("#customer-passport").val();
    var password = $("#customer-password").val();
    var birthday = $("#customer-birthday").val();
    var id = $("#customer-id").val();

    $.ajax({
        url: "/eecare_war/customer/update",
        method: "POST",
        data: {
            "id" : id,
            "firstName" : name,
            "lastName" : lastname,
            "address" : address,
            "email" : email,
            "passportData" : passport,
            "password": password,
            "birthday": birthday
            // "id" : id,
            // "firstname" : name,
            // "lastname" : lastname,
            // "address" : address,
            // "email" : email,
            // "passport" : passport
        },
        success: function (text) {
            var alreadyTakenEmailError = $(text).find('#already-taken-email');

            if (alreadyTakenEmailError.length > 0) {
                // console.log(text)
                $("#fail-update-phone").modal('show')
            }
            else {
                $("#success-update").modal('show')
                // window.location.href = "/eecare_war/customer/" + id
            }
        },
        error: function () {
            alert("FAILED")
        }
    })
}

function focusElement(document) {
    var email = $(document).find('#customer-email');
    email.focus()
}