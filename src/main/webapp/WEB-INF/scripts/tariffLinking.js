$(document).ready(function () {

    var linkingButton = $("#submit-linking");

    linkingButton.click(function () {

        var tariffId = $("#tariffId").val();

        var options = $("[name='tariffOption']", document)

        var optionList = []

        for (var i = 0; i < options.length; i++) {
            if (options.get(i).checked) {
                optionList.push(options.get(i).value)
            }
        }


        $.ajax({
            url: "http://localhost:8082/eecare_war/tariff/linking",
            method: "POST",
            data: {"tariff": tariffId,
                   "options" : optionList.toString()
            },
            success: function () {
                $("#success").modal('show')
                // window.location.href='/eecare_war/tariff/' + tariffId
            },
            error: function () {
                alert("ERROR")
            }
        })
    })
})