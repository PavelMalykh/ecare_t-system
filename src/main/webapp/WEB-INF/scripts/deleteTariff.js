function deleteTariff() {
    var tariffId = $("#tariff-id").val();

    $.ajax({
        url: "/eecare_war/tariff/block/" + tariffId,
        method: "POST",
        success: function (text) {
            $("#success").modal('show')
        }
    })
}