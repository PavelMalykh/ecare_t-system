$(document).ready(function () {

  var searchButton = $("#search-by-phone");

  $("#search-phone").mask("9-999-999-99-99");

  searchButton.click(function () {

    var phone = $("#search-phone").val();

    $.ajax({
      url: "/eecare_war/customer/phone",
      method: "POST",
      data: {"phone": phone
      },
      success: function (text) {
        var customerId = $(text).find("#customer-id").val()
        if (customerId != null) {
          window.location.href = "/eecare_war/customer/" + customerId
        } else {
          window.location.href = "/eecare_war/entityNotFound"
        }
      },
      error: function () {
        alert("ERROR")
      }
    })
  })
})

// var phone = $(text).find("#phone").val();
