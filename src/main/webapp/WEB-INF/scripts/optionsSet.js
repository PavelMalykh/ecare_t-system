function getOptionSet() {

    var optionId = $("#options-list").val();
    var incompatibleList = $("#incompatible-list");
    var obligatoryList = $("#obligatory-list")

    $.ajax({
        url: "/eecare_war/option/" + optionId,
        method: "GET",
        success: function (text) {

            var x = $(text).find("#obligatory-options").html();
            var y = $(text).find("#incompatible-options").html();

            console.log(x)
            console.log(y)

            incompatibleList.html(y)
            obligatoryList.html(x)
        }
    })

}

// var tariffId = $("#tariff").val();
//
// var optionList = $("#options-list");
//
// var optionListBlock = $("#options-list-block");
//
// if (tariffId === "") {
//     optionListBlock.hide()
// } else {
//
//     $.ajax({
//             url: "/eecare_war/tariff/" + tariffId,
//             method: "GET",
//             success: function (text) {
//                 var x = $(text).find("#options").html();
//
//                 // var options = console.log($(text).getElementsByName('tariffOption'))
//
//                 var z = $("[name='tariffOption']", text)
//
//                 for(var i = 0; i < z.length; i++) {
//                     z.get(i).disabled = false
//                     z.get(i).selected = false
//                 }
//                 console.log(z)
//
//                 if (x.trim() === "") {
//                     optionListBlock.hide()
//                 } else {
//                     optionListBlock.show();
//                     optionList.html(z);
//                 }
//             }
//         }
//     )
// }