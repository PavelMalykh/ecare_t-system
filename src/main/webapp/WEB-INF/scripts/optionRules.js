$(document).ready(function () {

    var linkingButton = $("#submit");

    linkingButton.click(function () {

        var mainOption = $("#main-option").val();
        var obligatoryOptions = $("#obligatory-options-list").val();
        if (obligatoryOptions === null) {
            obligatoryOptions = "";
        }
        var incompatibleOptions = $("#incompatible-options-list").val();
        if (incompatibleOptions === null) {
            incompatibleOptions = "";
        }

        $.ajax({
            url: "/eecare_war/option/rule",
            method: "POST",
            data: {"option": mainOption,
                "obligatoryOptions" : obligatoryOptions.toString(),
                "incompatibleOptions" : incompatibleOptions.toString()
            },
            success: function (text) {
                var intersectingError = $(text).find("#intersecting-error");

                if (intersectingError.length > 0) {
                    window.location.href = "/eecare_war/intersectingOptions"
                } else {
                    window.location.href = "/eecare_war/option/" + mainOption
                }
            },
            error: function () {
                alert("ERROR")
            }
        })
    })
})