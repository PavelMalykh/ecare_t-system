function getOptionsAndSetCard(text) {

    // console.log(tariffOptionNames)

    if (text.checked) {
        var optionId = text.value

        $.ajax({
            url: "/eecare_war/option/getOptions/" + optionId,
            method: "GET",
            success: function (text) {
                var obligatoryOptions = $("[name='ObligatoryOption']", text)
                var obligatoryOptionIds = []

                var incompatibleOptions = $("[name='incompatibleOption']", text)
                var incompatibleOptionIds = []

                // var obligatoryOptionNames = []

                for (var i = 0; i < incompatibleOptions.length; i++) {
                    incompatibleOptionIds.push(incompatibleOptions[i].value);
                }

                for (var i = 0; i < obligatoryOptions.length; i++) {
                    obligatoryOptionIds.push(obligatoryOptions[i].value);
                    // obligatoryOptionNames.push(obligatoryOptions[i].getAttribute("data-name"));
                }

                for (var i = 0; i < incompatibleOptions.length; i++) {
                    if (i != 0) {
                        document.querySelector("[name='tariffOption']" + "[value='" + incompatibleOptionIds[i] + "']").parentElement.style.opacity="0.4"
                        document.querySelector("[name='tariffOption']" + "[value='" + incompatibleOptionIds[i] + "']").disabled = true
                    }
                }

                for (var i = 0; i < obligatoryOptionIds.length; i++) {
                    if (i == 0) {
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").checked = true
                    } else {
                        // console.log(document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']"))
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").parentElement.className = 'toggle btn btn-outline-warning'
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").parentElement.style.opacity="0.4"
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").checked = true
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").disabled = true
                    }
                }

                var tariffOption = $("[name='tariffOption']", document)

                var tariffOptionNames = []

                for (var i = 0; i < tariffOption.length; i++) {
                    if (tariffOption.get(i).checked) {
                        tariffOptionNames.push(tariffOption.get(i).getAttribute('data-name'));
                    }
                }

                console.log(tariffOptionNames)


                var newList = document.createElement('ul')
                newList.id = "created-ul-id"
                newList.className = "created-ul"

                var element = document.getElementById('optionsBlock')
                element.innerHTML = "- Options:"

                element.appendChild(newList)

                for (var i = 0; i < tariffOptionNames.length; i++) {
                    var li = document.createElement('li')
                    li.id = "optionId" + tariffOptionNames[i]
                    li.innerHTML = tariffOptionNames[i];
                    newList.appendChild(li)
                }

                // document.querySelector("[id='optionsBlock']").innerHTML = '- Options: '
                // for (var i = 0; i < obligatoryOptionNames.length; i++) {
                //     document.querySelector("[id='optionsBlock']");
                //
                //
                //     // document.querySelector("[id='optionsBlock']").innerHTML = '- Options: ' + obligatoryOptionNames[i]
                // }

                // document.querySelector("[id='optionsBlock']").innerHTML = '- Options: '

            }
        })

    } else {
        var optionId = text.value

        $.ajax({
            url: "/eecare_war/option/getOptions/" + optionId,
            method: "GET",
            success: function (text) {
                var obligatoryOptions = $("[name='ObligatoryOption']", text)
                var obligatoryOptionIds = []
                // var obligatoryOptionNames = []
                for (var i = 0; i < obligatoryOptions.length; i++) {
                    obligatoryOptionIds.push(obligatoryOptions[i].value);
                    // obligatoryOptionNames.push(obligatoryOptions[i].getAttribute("data-name"));
                }

                var incompatibleOptions = $("[name='incompatibleOption']", text)
                var incompatibleOptionIds = []

                for (var i = 0; i < incompatibleOptions.length; i++) {
                    incompatibleOptionIds.push(incompatibleOptions[i].value);
                }

                for (var i = 0; i < incompatibleOptions.length; i++) {
                    if (i != 0) {
                        document.querySelector("[name='tariffOption']" + "[value='" + incompatibleOptionIds[i] + "']").parentElement.style.opacity="1"
                        document.querySelector("[name='tariffOption']" + "[value='" + incompatibleOptionIds[i] + "']").disabled = false
                    }
                }

                for (var i = 0; i < obligatoryOptionIds.length; i++) {
                    if (i == 0) {
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").checked = false
                    } else {
                        // console.log(document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']"))
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").parentElement.style.opacity = "1"
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").parentElement.className = 'toggle btn btn-outline-info off'
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").checked = false
                        document.querySelector("[name='tariffOption']" + "[value='" + obligatoryOptionIds[i] + "']").disabled = false
                    }
                }

                var tariffOption = $("[name='tariffOption']", document)

                var tariffOptionNames = []

                for (var i = 0; i < tariffOption.length; i++) {
                    if (tariffOption.get(i).checked) {
                        tariffOptionNames.push(tariffOption.get(i).getAttribute('data-name'));
                    }
                }

                // console.log(tariffOptionId)


                var createUl = document.getElementById("created-ul-id")
                // var newList = document.createElement('ul')
                // newList.className = "created-ul"
                //
                // var element = document.getElementById('optionsBlock')
                // element.innerHTML = "- Options:"
                //
                // element.appendChild(newList)
                if (tariffOptionNames.length > 0) {

                    var newList = document.createElement('ul')
                    newList.id = "created-ul-id"
                    newList.className = "created-ul"

                    var element = document.getElementById('optionsBlock')
                    element.innerHTML = "- Options:"

                    element.appendChild(newList)

                    for (var i = 0; i < tariffOptionNames.length; i++) {
                        var li = document.createElement('li')
                        li.id = "optionId" + tariffOptionNames[i]
                        li.innerHTML = tariffOptionNames[i];
                        newList.appendChild(li)
                    }

                //     for (var i = 0; i < tariffOptionNames.length; i++) {
                //         var deletedLi = document.getElementById('optionId' + tariffOptionNames[i])
                //         createUl.removeChild(deletedLi)
                //     }
                // } else {
                //     document.getElementById("optionsBlock").innerText = "- No options were selected"
                } else {
                    document.getElementById("optionsBlock").innerText = "- No options were selected"
                }


                // document.getElementById("optionsBlock").innerText = "- No options were selected"

            }

        })


    }
}

function finalList(text) {

}